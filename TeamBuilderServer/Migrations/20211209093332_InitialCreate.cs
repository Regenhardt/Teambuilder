using Microsoft.EntityFrameworkCore.Migrations;
using System;

// Generated code
#pragma warning disable IDE0053 // Use expression body for lambda expressions
#nullable disable

namespace TeamBuilderServer.Migrations;

public partial class InitialCreate : Migration
{
	protected override void Up(MigrationBuilder migrationBuilder)
	{
		migrationBuilder.CreateTable(
			name: "Players",
			columns: table => new
			{
				PlayerID = table.Column<short>(type: "smallint", nullable: false)
					.Annotation("SqlServer:Identity", "1, 1"),
				Name = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
				Password = table.Column<byte[]>(type: "varbinary(50)", unicode: false, maxLength: 50, nullable: false),
				Rank = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
			},
			constraints: table =>
			{
				table.PrimaryKey("PK__Players__4A4E74A90790B775", x => x.PlayerID)
					.Annotation("SqlServer:Clustered", false);
			});

		migrationBuilder.CreateTable(
			name: "Teams",
			columns: table => new
			{
				TeamID = table.Column<int>(type: "int", nullable: false)
					.Annotation("SqlServer:Identity", "1, 1"),
				Name = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: false),
				Owner = table.Column<short>(type: "smallint", nullable: false)
			},
			constraints: table =>
			{
				table.PrimaryKey("PK__Teams__123AE7B807D38443", x => x.TeamID)
					.Annotation("SqlServer:Clustered", false);
			});

		migrationBuilder.CreateTable(
			name: "LoginTokens",
			columns: table => new
			{
				Token = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
				Player = table.Column<short>(type: "smallint", nullable: false)
			},
			constraints: table =>
			{
				table.PrimaryKey("PK__LoginTok__1EB4F8174D7F3FAE", x => x.Token)
					.Annotation("SqlServer:Clustered", false);
				table.ForeignKey(
					name: "FK__LoginToke__Playe__2116E6DF",
					column: x => x.Player,
					principalTable: "Players",
					principalColumn: "PlayerID");
			});

		migrationBuilder.CreateTable(
			name: "TeamMembers",
			columns: table => new
			{
				Team = table.Column<int>(type: "int", nullable: false),
				Player = table.Column<short>(type: "smallint", nullable: false),
				HasConfirmed = table.Column<bool>(type: "bit", nullable: false)
			},
			constraints: table =>
			{
				table.PrimaryKey("PK_TeamMembers", x => new { x.Team, x.Player })
					.Annotation("SqlServer:Clustered", false);
				table.ForeignKey(
					name: "FK__TeamMembe__Playe__220B0B18",
					column: x => x.Player,
					principalTable: "Players",
					principalColumn: "PlayerID");
				table.ForeignKey(
					name: "FK__TeamMember__Team__1A69E950",
					column: x => x.Team,
					principalTable: "Teams",
					principalColumn: "TeamID");
			});

		migrationBuilder.CreateIndex(
			name: "IX_LoginTokens_Player",
			table: "LoginTokens",
			column: "Player");

		migrationBuilder.CreateIndex(
			name: "IX_TeamMembers_Player",
			table: "TeamMembers",
			column: "Player");
	}

	protected override void Down(MigrationBuilder migrationBuilder)
	{
		migrationBuilder.DropTable(
			name: "LoginTokens");

		migrationBuilder.DropTable(
			name: "TeamMembers");

		migrationBuilder.DropTable(
			name: "Players");

		migrationBuilder.DropTable(
			name: "Teams");
	}
}
