using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TeambuilderLibrary.Contracts;
using TeambuilderLibrary.Contracts.Account;
using TeambuilderLibrary.Contracts.Team;
using TeamBuilderServer.Database;

namespace TeamBuilderServer;

[PublicAPI]
public static class DependencyInjection
{
	private const string StandardConnectionString =
		"Data Source=(LocalDB)\\MSSQLLocalDB;Integrated Security=True;MultipleActiveResultSets=True;";

	/// <summary>
	/// Add the database with the given connection string to the DI container
	/// </summary>
	/// <param name="services">The DI container to use for repositories</param>
	/// <param name="connectionString">Used to choose and connect to the database</param>
	/// <returns>The service collection object for chaining</returns>
	public static IServiceCollection AddDatabase(this IServiceCollection services, string? connectionString = null)
	{
		services.AddDbContextPool<Entities>(options =>
		{
			options.EnableDetailedErrors();
			options.UseSqlServer(connectionString ?? StandardConnectionString);
		});
		services.AddTransient<IAccountRepository, AccountDbAccess>();
		services.AddTransient<ITeamRepository, TeamDbAccess>();
		services.AddSingleton<ICryptoService, CryptoService>();
		return services;
	}

	/// <summary>
	/// Makes sure the database is up to date.
	/// </summary>
	/// <param name="provider">Service provider to retrieve the DbContext previously registered using <seealso cref="AddDatabase(IServiceCollection, string?)"/></param>
	public static void UpdateDatabase(this IServiceProvider provider)
	{
		using var scope = provider.CreateScope();
		scope.ServiceProvider.GetRequiredService<Entities>().Database.Migrate();
	}
}
