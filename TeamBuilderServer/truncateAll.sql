CREATE PROCEDURE TruncateAll
AS
BEGIN
	--truncate non fk tables
	TRUNCATE TABLE LoginTokens;
	TRUNCATE TABLE TeamMembers;

	--truncate fk tables
	DELETE FROM Teams
	DBCC CHECKIDENT ('dbo.Teams',RESEED, 0);

	DELETE FROM Players
	DBCC CHECKIDENT ('dbo.Players',RESEED, 0);

	--insert test values
	INSERT INTO Players VALUES ('Dummy Player', '123', 3);
	INSERT INTO Players VALUES ('Dummy Player 1', '123', 4);
	INSERT INTO Players VALUES ('Dummy Player 2', '123', 5);
	INSERT INTO Teams VALUES ('My Team 1', 1);
	INSERT INTO Teams VALUES ('My Team 2', 1);
	INSERT INTO Teams VALUES ('My Team 3', 1);
	INSERT INTO Teams VALUES ('My Team', 1);
	INSERT INTO TeamMembers VALUES (1, 1, 0);
	INSERT INTO TeamMembers VALUES (1, 2, 0);
	INSERT INTO TeamMembers VALUES (1, 3, 0);
	INSERT INTO TeamMembers VALUES (2, 2, 0);
	INSERT INTO TeamMembers VALUES (3, 3, 0);
	INSERT INTO TeamMembers VALUES (4, 1, 0);
END