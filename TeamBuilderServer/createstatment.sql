DROP TABLE TeamMembers;
DROP TABLE TeamNames;
DROP TABLE LoginTokens;
DROP TABLE Players;
DROP TABLE [RANKS];

--The available rankings
CREATE TABLE [Ranks]
(
	RankID		INTEGER 		NOT NULL PRIMARY KEY IDENTITY,
	Title		varchar(15)		NOT NULL 
);

--registered playes
CREATE TABLE Players
(
	[PlayerID] 	INTEGER 		NOT NULL PRIMARY KEY IDENTITY,
	[Name]		varchar(50) 	NOT NULL,
	[Password]	varchar(50)		NOT NULL,
	[Rank]		INTEGER 		NOT NULL,
	Foreign Key ([Rank]) REFERENCES [Ranks](RankID)
);

--Login tokens
CREATE TABLE LoginTokens
(
	[TokenID]	INTEGER 		NOT NULL PRIMARY KEY IDENTITY,
	[Player]	INTEGER		 	NOT NULL,
	[Token]		varchar(max)	NOT NULL,

	Foreign Key ([Player]) REFERENCES Players(PlayerID)
);

--team templates
CREATE TABLE TeamNames
(
	[TeamID] 	INTEGER 		NOT NULL PRIMARY KEY IDENTITY,
	[Name]		varchar(100) 	NOT NULL,	
	[Owner]		INTEGER		 	NOT NULL,

	Foreign Key ([Owner]) REFERENCES Players(PlayerID)
);

--Memerbs from the teams
CREATE TABLE TeamMembers
(
	[EntryID] 	INTEGER 		NOT NULL PRIMARY KEY IDENTITY,
	[Team]		INTEGER			NOT NULL,
	[Player]	INTEGER			NOT NULL,
	[HasConfirmed] BIT			DEFAULT(0),
	
	Foreign Key ([Player]) REFERENCES Players(PlayerID),	
	Foreign Key ([Team]) REFERENCES TeamNames(TeamID)	
);

--insert the default rankings
SET IDENTITY_INSERT [Ranks] ON;
INSERT INTO [Ranks]([RankID], [Title]) VALUES (0, 'Unranked');
INSERT INTO [Ranks]([RankID], [Title]) VALUES (1, 'Herald');
INSERT INTO [Ranks]([RankID], [Title]) VALUES (2, 'Guardian');
INSERT INTO [Ranks]([RankID], [Title]) VALUES (3, 'Crusader');
INSERT INTO [Ranks]([RankID], [Title]) VALUES (4, 'Archon');
INSERT INTO [Ranks]([RankID], [Title]) VALUES (5, 'Legend');
INSERT INTO [Ranks]([RankID], [Title]) VALUES (6, 'Ancient');
INSERT INTO [Ranks]([RankID], [Title]) VALUES (7, 'Divine');

INSERT INTO Players VALUES ('Dummy Player', '123', 3);
INSERT INTO Players VALUES ('Dummy Player 1', '123', 3);
INSERT INTO Players VALUES ('Dummy Player 2', '123', 3);
INSERT INTO TeamNames VALUES ('My Team 1', 1);
INSERT INTO TeamNames VALUES ('My Team 2', 1);
INSERT INTO TeamNames VALUES ('My Team 3', 1);
INSERT INTO TeamNames VALUES ('My Team', 1);
INSERT INTO TeamMembers VALUES (1, 1, 0);
INSERT INTO TeamMembers VALUES (1, 2, 0);
INSERT INTO TeamMembers VALUES (1, 3, 0);
INSERT INTO TeamMembers VALUES (2, 2, 0);
INSERT INTO TeamMembers VALUES (3, 3, 0);
INSERT INTO TeamMembers VALUES (4, 1, 0);

