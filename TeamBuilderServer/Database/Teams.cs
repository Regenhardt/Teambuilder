namespace TeamBuilderServer.Database;

public sealed partial class Teams
{
#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
	public Teams()
	{
		TeamMembers = [];
	}

	public int TeamId { get; set; }
	public string Name { get; set; }
	public short Owner { get; set; }

	public ICollection<TeamMembers> TeamMembers { get; set; }
}
