namespace TeamBuilderServer.Database;

public class LoginTokens
{
	public Guid Token { get; set; }
	public short Player { get; set; }

	public virtual Players PlayerNavigation { get; set; } = null!;
}
