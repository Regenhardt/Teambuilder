using Microsoft.EntityFrameworkCore;

namespace TeamBuilderServer.Database;

internal class Entities(DbContextOptions<Entities> options) : DbContext(options)
{
	public virtual DbSet<LoginTokens> LoginTokens { get; set; } = null!;
	public virtual DbSet<Players> Players { get; set; } = null!;
	public virtual DbSet<TeamMembers> TeamMembers { get; set; } = null!;
	public virtual DbSet<Teams> Teams { get; set; } = null!;

	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		modelBuilder.Entity<LoginTokens>(entity =>
		{
			entity.HasKey(e => e.Token)
				.HasName("PK__LoginTok__1EB4F8174D7F3FAE")
				.IsClustered(false);

			entity.Property(e => e.Token).ValueGeneratedNever();

			entity.HasOne(d => d.PlayerNavigation)
				.WithMany(p => p.LoginTokens)
				.HasForeignKey(d => d.Player)
				.OnDelete(DeleteBehavior.ClientSetNull)
				.HasConstraintName("FK__LoginToke__Playe__2116E6DF");
		});

		modelBuilder.Entity<Players>(entity =>
		{
			entity.HasKey(e => e.PlayerId)
				.HasName("PK__Players__4A4E74A90790B775")
				.IsClustered(false);

			entity.Property(e => e.PlayerId).HasColumnName("PlayerID");

			entity.Property(e => e.Name)
				.IsRequired()
				.HasMaxLength(50)
				.IsUnicode(false);

			entity.Property(e => e.Password)
				.IsRequired()
				.HasMaxLength(50)
				.IsUnicode(false);

			entity.Property(e => e.Rank)
				.IsRequired()
				.HasMaxLength(50);
		});

		modelBuilder.Entity<TeamMembers>(entity =>
		{
			entity.HasKey(e => new { e.Team, e.Player })
				.IsClustered(false);

			entity.HasOne(d => d.PlayerNavigation)
				.WithMany(p => p.TeamMembers)
				.HasForeignKey(d => d.Player)
				.OnDelete(DeleteBehavior.ClientSetNull)
				.HasConstraintName("FK__TeamMembe__Playe__220B0B18");

			entity.HasOne(d => d.TeamNavigation)
				.WithMany(p => p.TeamMembers)
				.HasForeignKey(d => d.Team)
				.OnDelete(DeleteBehavior.ClientSetNull)
				.HasConstraintName("FK__TeamMember__Team__1A69E950");
		});

		modelBuilder.Entity<Teams>(entity =>
		{
			entity.HasKey(e => e.TeamId)
				.HasName("PK__Teams__123AE7B807D38443")
				.IsClustered(false);

			entity.Property(e => e.TeamId).HasColumnName("TeamID");

			entity.Property(e => e.Name)
				.IsRequired()
				.HasMaxLength(100)
				.IsUnicode(false);
		});
	}

	public virtual int TruncateAll() => Database.ExecuteSqlRaw(File.ReadAllText("truncateAll.sql"));
}
