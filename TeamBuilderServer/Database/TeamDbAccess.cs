using Microsoft.EntityFrameworkCore;
using TeambuilderLibrary.Contracts.Team;
using TeambuilderLibrary.Models;

// EF async overloads are often for EF generation only:
// ReSharper disable MethodHasAsyncOverload

namespace TeamBuilderServer.Database;

internal class TeamDbAccess(Entities entities) : ITeamRepository
{
	private readonly Entities entities = entities;

	public async IAsyncEnumerable<Team> GetAllTeams()
	{
		foreach (var t in entities.Teams)
		{
			yield return await ConvertToModel(t);
		}
	}

	private async Task<IList<Player>> GetPlayersForTeam(int teamId) => await entities.Players.Include(p => p.TeamMembers).Where(p => p.TeamMembers.Any(m => m.Team == teamId)).AsNoTracking().Cast<Player>().ToListAsync().ConfigureAwait(false);

	public async Task<Result<int>> GetTeamIdByName(string teamName)
	{
		try
		{
			return Result<int>.Positive((await entities.Teams.AsNoTracking().SingleAsync(t => t.Name == teamName)).TeamId);
		}
		catch (Exception e)
		{
			return Result<int>.Negative(e.Message);
		}
	}

	public async Task<Team> GetTeam(int teamId) => await ConvertToModel(await entities.Teams.AsNoTracking().SingleAsync(t => t.TeamId == teamId));

	public async Task<Team> GetTeam(string teamName) => await ConvertToModel(await entities.Teams.AsNoTracking().SingleAsync(t => t.Name == teamName));

	public async Task<Result<Team>> CreateNewTeam(short ownerId, string teamName)
	{
		if (await entities.Teams.AsNoTracking().AnyAsync(t => t.Name == teamName))
		{
			return Result<Team>.Negative("Team name already in use");
		}
		var newTeam = entities.Teams.Add(new Teams { Owner = ownerId, Name = teamName }).Entity;
		await entities.SaveChangesAsync();

		return Result<Team>.Positive(await ConvertToModel(newTeam));
	}

	public async Task DeleteTeam(short ownerId, string teamName)
	{
		if (!await PlayerOwnsTeam(ownerId, teamName))
		{
			throw new InvalidOperationException("Player not owner of team or team not real");
		}

		await using var transaction = await entities.Database.BeginTransactionAsync();
		var team = await entities.Teams.Include(t => t.TeamMembers).SingleAsync(t => t.Name == teamName);
		team.TeamMembers.Clear();
		entities.Teams.Remove(team);
		await entities.SaveChangesAsync();
		await transaction.CommitAsync();
	}

	private async Task<bool> PlayerOwnsTeam(short playerId, string teamName) => await entities.Teams.AsNoTracking().AnyAsync(t => t.Name == teamName && t.Owner == playerId);

	public async Task<Result> AddPlayerToTeam(short playerId, int teamId)
	{
		if (await entities.TeamMembers.AnyAsync(tm => tm.Player == playerId && tm.Team == teamId))
		{
			return Result.Negative("Player is already part of the team");
		}

		entities.TeamMembers.Add(
			new TeamMembers
			{
				HasConfirmed = true,
				Player = playerId,
				Team = teamId
			});

		await entities.SaveChangesAsync();

		return Result.Positive();
	}

	public async Task<Result> RemovePlayerFromTeam(short playerId, string teamName)
	{
		if (!await TeamHasPlayer(teamName, playerId))
		{
			return Result.Negative("Player is not part of that team");
		}

		entities.TeamMembers.Remove(await entities.TeamMembers.SingleAsync(tm =>
			tm.Player == playerId && tm.TeamNavigation.Name == teamName));

		await entities.SaveChangesAsync();

		return Result.Positive();
	}

	private async Task<bool> TeamHasPlayer(string teamName, short playerId)
	{
		return await entities.TeamMembers.AsNoTracking()
			.AnyAsync(tm => tm.Player == playerId && tm.TeamNavigation.Name == teamName);
	}

	private async Task<Team> ConvertToModel(Teams team)
	{
		return new Team
		{
			Members = await GetPlayersForTeam(team.TeamId),
			Name = team.Name,
			Owner = await entities.Players.SingleAsync(p => p.PlayerId == team.Owner)
		};
	}
}
