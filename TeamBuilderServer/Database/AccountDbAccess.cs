using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Security.Authentication;
using TeambuilderLibrary.Contracts.Account;
using TeambuilderLibrary.Models;

namespace TeamBuilderServer.Database;

internal class AccountDbAccess(Entities entities) : IAccountRepository
{
	private readonly Entities entities = entities;

	public async Task<bool> AccountExists(string userName) => await entities.Players.AnyAsync(p => p.Name == userName);

	public async Task<bool> ValidateToken(Guid loginToken) => await entities.LoginTokens.AsNoTracking().AnyAsync(t => t.Token == loginToken);

	public async Task RegisterNewAccountAsync(string username, byte[] passwordHash, Rank rank)
	{
		if (passwordHash.Length > 256)
			throw new ArgumentOutOfRangeException(nameof(passwordHash), "Hash may be 256 bit maximum.");

		entities.Players.Add(new Players { Name = username, Password = passwordHash, Rank = rank.ToString() });
		await entities.SaveChangesAsync();
	}

	public async Task<Player> GetPlayer(string username) => await entities.Players.AsNoTracking().SingleAsync(p => p.Name == username);

	public async Task<Player> GetPlayerFromToken(Guid token)
	{
		return !await ValidateToken(token)
			? throw new SecurityTokenValidationException()
			: (Player)(await entities.LoginTokens.Include(t => t.PlayerNavigation).AsNoTracking()
				.FirstAsync(t => t.Token == token))
			.PlayerNavigation;
	}

	public async Task<Player> GetPlayerFromCredentials(string username, byte[] password)
	{
		return await entities.Players.AsNoTracking().SingleOrDefaultAsync(p => p.Name == username && p.Password == password) ??
			   throw new InvalidCredentialException("Username or password incorrect.");
	}

	public async Task<Guid> GetNewTokenForPlayer(string username)
	{
		var token = Guid.NewGuid();
		var player = await entities.Players.SingleAsync(p => p.Name == username);
		await entities.LoginTokens.AddAsync(new LoginTokens
		{
			Player = player.PlayerId,
			PlayerNavigation = player,
			Token = token
		});
		await entities.SaveChangesAsync();
		return token;
	}

	public async Task RemoveLoginToken(Guid guidToken)
	{
		var entityToDelete = await entities.LoginTokens.FindAsync(guidToken);
		if (entityToDelete != null)
		{
			entities.Remove(entityToDelete);
			await entities.SaveChangesAsync();
		}
	}

	public async Task RemoveAllTokensFor(Guid currentToken)
	{
		var playerId = await entities.LoginTokens.AsNoTracking().Where(lt => lt.Token == currentToken).Select(lt => lt.Player)
			.FirstAsync();
		entities.LoginTokens.RemoveRange(entities.LoginTokens.Where(lt => lt.Player == playerId));
		await entities.SaveChangesAsync();
	}

	public async Task<bool> ValidateActiveUserData(string deletionDataUsername, byte[] passwordHash, Guid token)
	{
		return await entities.LoginTokens.AsNoTracking().AnyAsync(t =>
			t.Token == token && t.PlayerNavigation.Name == deletionDataUsername &&
			t.PlayerNavigation.Password == passwordHash);
	}

	public async Task DeleteAccount(string username)
	{
		var activeTokens = entities.LoginTokens.Where(t => t.PlayerNavigation.Name == username);
		entities.LoginTokens.RemoveRange(await activeTokens.ToArrayAsync());
		var accountToDelete = await entities.Players.SingleAsync(p => p.Name == username);
		entities.Players.Remove(accountToDelete);
		await entities.SaveChangesAsync();
	}

	public async Task ChangeName(short playerId, string newName)
	{
		var player = await entities.Players.SingleAsync(p => p.PlayerId == playerId);
		player.Name = newName;
		await entities.SaveChangesAsync();
	}

	public async Task ChangeRank(short playerId, Rank newRank)
	{
		var player = await entities.Players.SingleAsync(p => p.PlayerId == playerId);
		player.Rank = newRank.ToString();
		await entities.SaveChangesAsync();
	}

	public async Task<short> GetPlayerIdFromToken(Guid currentToken) => (await entities.LoginTokens.AsNoTracking().SingleAsync(t => t.Token == currentToken)).Player;
}
