namespace TeamBuilderServer.Database;

public sealed partial class Players
{
#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
	public Players()
	{
		LoginTokens = [];
		TeamMembers = [];
	}

	public short PlayerId { get; set; }
	public string Name { get; set; }
	public byte[] Password { get; set; }
	public string Rank { get; set; }

	public ICollection<LoginTokens> LoginTokens { get; set; }
	public ICollection<TeamMembers> TeamMembers { get; set; }
}
