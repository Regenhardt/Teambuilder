using TeambuilderLibrary.Models;

namespace TeamBuilderServer.Database;

sealed partial class Players
{
	public override string ToString() => Name;

	public static implicit operator Player(Players p) => new(p.Name, Enum.Parse<Rank>(p.Rank));
}

sealed partial class Teams
{
	public override string ToString() => Name;
}

partial class TeamMembers
{
}




