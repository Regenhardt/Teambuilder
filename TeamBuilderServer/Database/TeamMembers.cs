namespace TeamBuilderServer.Database;

public partial class TeamMembers
{
	public int Team { get; set; }
	public short Player { get; set; }
	public bool HasConfirmed { get; set; }

	public virtual Players PlayerNavigation { get; set; } = null!;
	public virtual Teams TeamNavigation { get; set; } = null!;
}
