using System.Security.Cryptography;
using System.Text;
using TeambuilderLibrary.Contracts;

namespace TeamBuilderServer;

internal class CryptoService : ICryptoService
{
	public byte[] Hash(string password)
	{
		var clearBytes = Encoding.UTF8.GetBytes(password);
		var hashedBytes = SHA256.HashData(clearBytes);
		return hashedBytes;
	}
}
