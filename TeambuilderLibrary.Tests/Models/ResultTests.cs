using Microsoft.VisualStudio.TestTools.UnitTesting;
using TeambuilderLibrary.Models;

namespace TeambuilderLibrary.Tests.Models;

[TestClass]
public class ResultTests
{
	[TestMethod]
	public void ToString_ShouldIncludeAllData()
	{
		const string data = "I'm some data";
		const bool success = true;
		const string message = "I'm a message";
		var resultObject = new Result<string>
		{
			Data = data,
			Message = message,
			Success = success
		};

		var output = resultObject.ToString();

		Assert.IsTrue(output.Contains(data));
		Assert.IsTrue(output.Contains(message));
		Assert.IsTrue(output.Contains(success.ToString()));
	}
}
