using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Runtime.CompilerServices;
using TeambuilderLibrary.Models;

namespace TeambuilderLibrary.Tests.Models;

[TestClass]
public class PlayerTests
{
	[MethodImpl(MethodImplOptions.NoOptimization)]
	[TestMethod]
	public void Equals_WithSamePlayer_ShouldReturnTrue()
	{
		var player = new Player("ImaPlayer", Rank.Ancient);
		var playerAsObject = (object)player;

		Assert.IsTrue(player.Equals(playerAsObject));
	}

	[MethodImpl(MethodImplOptions.NoOptimization)]
	[TestMethod]
	public void Equals_WithSamePlayerData_ShouldReturnTrue()
	{
		var player1 = new Player("Player", Rank.Legend);
		var player2 = new Player("Player", Rank.Legend);

		Assert.IsTrue(player1.Equals(player2));
	}

	[TestMethod]
	public void GetHashCode_ShouldWork()
	{
		var player1 = new Player("Player", Rank.Legend);
		var player2 = new Player("Player", Rank.Legend);

		Assert.AreEqual(player1.GetHashCode(), player2.GetHashCode());
	}
}
