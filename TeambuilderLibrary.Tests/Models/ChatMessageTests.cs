using Microsoft.VisualStudio.TestTools.UnitTesting;
using TeambuilderLibrary.Models;

namespace TeambuilderLibrary.Tests.Models;

[TestClass]
public class ChatMessageTests
{
	private const string Sender = "ImaSender";
	private const string Message = "ImaMessage";
	[TestMethod]
	public void New_ShouldReturnNewEmptyAnonymousMessage()
	{
		var message = ChatMessage.New;

		Assert.AreEqual("Anonymous", message.Sender);
		Assert.AreEqual(0, message.Content.Length, "Message not empty");
	}

	[TestMethod]
	public void ToString_ShouldIncludeData()
	{
		var message = ChatMessage.New;
		message.Sender = Sender;
		message.Content = Message;

		var output = message.ToString();

		Assert.IsTrue(output.Contains(Sender), "Sender not included");
		Assert.IsTrue(output.Contains(Message), "Message text not included");
	}
}
