using Microsoft.VisualStudio.TestTools.UnitTesting;
using TeambuilderLibrary.Extensions;
using TeambuilderLibrary.Models;

namespace TeambuilderLibrary.Tests.Extensions;

[TestClass]
public class TaskExtensionsTests
{
	[TestMethod]
	public async Task Data_ShouldDeliverDataAsync()
	{
		var data = new object();
		var resultObject = Result<object>.Positive(data);
		var task = Task.FromResult(resultObject);

		var result = await task.Data();

		Assert.AreSame(data, result);
	}
}
