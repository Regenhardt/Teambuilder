using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TeamBuilderServer.Tests;

[TestClass]
public class CryptoServiceTests
{
	[TestMethod]
	public void HashTest()
	{
		CryptoService cryptoService = new();
		var password = "ImaP@ssword";

		var hashes = new byte[][] { cryptoService.Hash(password), cryptoService.Hash(password), cryptoService.Hash(password) };
		var differentHash = cryptoService.Hash(password+"1");
		
		CollectionAssert.AreEqual(hashes[0], hashes[1]);
		CollectionAssert.AreEqual(hashes[0], hashes[2]);
		CollectionAssert.AreNotEqual(hashes[1], differentHash);
	}
}
