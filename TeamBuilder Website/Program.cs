using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.AspNetCore.SignalR.Client;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Components.Web;
using TeamBuilder_Website.Services;
using TeamBuilder_Website.Shared;
using TeambuilderLibrary.Contracts.Account;
using TeambuilderLibrary.Contracts.Team;

namespace TeamBuilder_Website;

public class Program
{
	internal static readonly string Build = GetBuildDate(Assembly.GetExecutingAssembly());

	public static async Task Main(string[] args)
	{
		var builder = WebAssemblyHostBuilder.CreateDefault(args);
		// Use domain from browser as default
		var baseAddress = builder.Configuration.GetValue("BaseAddress", builder.HostEnvironment.BaseAddress)!;
		builder.RootComponents.Add<App>("#app");
		builder.RootComponents.Add<HeadOutlet>("head::after");

		builder.Services.AddSingleton(_ =>
		{
			var opts = new JsonSerializerOptions();
			opts.Converters.Add(new JsonStringEnumConverter());
			return opts;
		});
		builder.Services.AddSingleton<AlertService>();
		builder.Services.AddSingleton(sp => new HttpClient { BaseAddress = new Uri(baseAddress) });
		builder.Services.AddSingleton<LocalStorage>();
		builder.Services.AddSingleton<AppState>();
		builder.Services.AddSingleton<TeambuilderHub>();
		builder.Services.AddSingleton<IAccountService, AccountService>();
		builder.Services.AddSingleton<ITeamService, TeamService>();
		builder.Services.AddSingleton<ChatService>();
		builder.Services.AddHubConnection(baseAddress);
		builder.Services.AddSingleton<SantaService>();

		var host = builder.Build();
		await host.RunAsync();
	}

	private static string GetBuildDate(Assembly assembly)
	{
		const string buildVersionMetadataPrefix = "+build";
		var attribute = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>();
		if (attribute?.InformationalVersion != null)
		{
			var value = attribute.InformationalVersion;
			var index = value.IndexOf(buildVersionMetadataPrefix, StringComparison.Ordinal);
			if (index > 0)
			{
				return value[(index + buildVersionMetadataPrefix.Length)..];
			}
		}

		return "<Error in GetBuildDate>";
	}
}

internal static class BuildExtensions
{
	internal static void AddHubConnection(this IServiceCollection services, string baseAddress) 
		=> services.AddSingleton(_ => new HubConnectionBuilder().WithUrl($"{baseAddress}signalr").WithAutomaticReconnect().Build());
}
