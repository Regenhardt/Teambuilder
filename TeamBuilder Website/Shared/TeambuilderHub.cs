using Microsoft.AspNetCore.SignalR.Client;
using TeamBuilder_Website.Services;
using TeambuilderLibrary.Contracts.Hub;
using TeambuilderLibrary.Extensions;
using TeambuilderLibrary.Models;

namespace TeamBuilder_Website.Shared;

public class TeambuilderHub : IServerHub, IAsyncDisposable, IClientHub
{
	private readonly HubConnection connection;
	private readonly AlertService alerts;
	private readonly AppState state;


	public event Action<ChatMessage>? OnMessage;
	public event Action<Team>? OnNewTeam;
	public event Action<string, Team>? OnPatchTeam;
	public event Action<string>? OnDeleteTeam;

	#region [ Init ]

	public TeambuilderHub(AppState state, HubConnection connection, AlertService alerts)
	{
		this.state = state;

		this.connection = connection;
		this.alerts = alerts;
		connection.Closed += e => OnConnectionClosed(e);
		connection.Reconnected += OnReconnected;
		connection.Reconnecting += e => SetConnectionState(e?.Message);
		InitMethods();
		StartConnection().Forget();
	}

	private async Task OnConnectionClosed(Exception? e)
	{
		await SetConnectionState();
		if (e != null)
		{
			alerts.Error($"Real time connection closed: {e.Message}\nAttempting reconnection.");
		}
		StartConnection().Forget();
	}

	private async Task OnReconnected(string? newConnectionId)
	{
		if (newConnectionId != null && state.IsLoggedIn)
		{
			await Login(state.Token);
		}
	}

	private Task SetConnectionState(string? msg = null)
	{
		var newState = Enum.Parse<LiveConnectionState>(connection.State.ToString());
		Console.WriteLine($"New SignalR state: {newState}");
		if (msg != null) Console.WriteLine($"Message: {msg}");
		state.ConnectionState = newState;
		return Task.CompletedTask;
	}

	private void InitMethods()
	{
		connection.On<Team>(nameof(IClientHub.ReceiveTeam), ReceiveTeam);
		connection.On<ChatMessage>(nameof(IClientHub.ReceiveMessage), ReceiveMessage);
		connection.On<string, Team>(nameof(IClientHub.PatchTeam), PatchTeam);
		connection.On<string>(nameof(IClientHub.RemoveTeam), RemoveTeam);
		connection.On<Player, Player>(nameof(IClientHub.SanteeIs), (santa, santee) => SanteeIs(santee));
		Console.WriteLine("SignalR initialized");
	}

	private async Task StartConnection()
	{
		if (connection.State == HubConnectionState.Disconnected)
		{
			await connection.StartAsync();
			await SetConnectionState();
		}
		else
		{
			Console.WriteLine($"Not starting, state is {connection.State}");
		}
		Console.WriteLine("Connection established");

		if (state.IsLoggedIn)
		{
			await Login(state.Token);
		}
	}

	public ValueTask DisposeAsync()
	{
		GC.SuppressFinalize(this);
		return connection.DisposeAsync();
	}

	#endregion

	public async Task<Result<string>> Login(Guid loginToken) => await connection.InvokeAsync<Result<string>>(nameof(IServerHub.Login), loginToken).ConfigureAwait(false);

	/// <summary>
	/// Make sure to be subscribed to <see cref="OnNewTeam"/> before calling this.
	/// </summary>
	public async Task SubscribeToAndReceiveTeamsAsync() => await connection.InvokeAsync(nameof(IServerHub.SubscribeToAndReceiveTeamsAsync));

	public async Task<Result> LeaveTeam(string teamName) => await connection.InvokeAsync<Result>(nameof(IServerHub.LeaveTeam), teamName);

	#region [ Team ]

	/// <summary>
	/// Only to be called by signalR, not by the application.
	/// Subscribe to <see cref="OnNewTeam"/> to get current and new teams as they get added.
	/// </summary>
	/// <param name="team"></param>
	/// <returns></returns>
	public Task ReceiveTeam(Team team)
	{
		OnNewTeam?.Invoke(team);
		return Task.CompletedTask;
	}

	public async Task<Result> JoinTeam(string teamName) => await connection.InvokeAsync<Result>(nameof(IServerHub.JoinTeam), teamName);

	public async Task<Result> CreateTeam(string teamName) => await connection.InvokeAsync<Result>(nameof(IServerHub.CreateTeam), teamName);

	public async Task<Result> DeleteTeam(string teamName) => await connection.InvokeAsync<Result>(nameof(IServerHub.DeleteTeam), teamName);

	public Task PatchTeam(string teamName, Team team)
	{
		Console.WriteLine($"Team {teamName} patched from Server:\n{team}");
		OnPatchTeam?.Invoke(teamName, team);
		return Task.CompletedTask;
	}

	public Task RemoveTeam(string teamname)
	{
		OnDeleteTeam?.Invoke(teamname);
		return Task.CompletedTask;
	}

	#endregion

	#region [ Chat ]

	public Task ReceiveMessage(ChatMessage msg)
	{
		OnMessage?.Invoke(msg);
		return Task.CompletedTask;
	}

	public async Task SendChatMessage(ChatMessage message) => await connection.InvokeAsync(nameof(IServerHub.SendChatMessage), message);

	#endregion

	#region [ Misc ]

	public async Task StartSecretSantaForTeam(Team team)
	{
		Console.WriteLine("Sending secret santa request to backend");
		await connection.InvokeAsync(nameof(IServerHub.StartSecretSantaForTeam), team);
	}

	/// <summary>
	/// Signals that a round of secret santa was triggered. First parameter is santa, second parameter is santee.
	/// </summary>
	public event Action<Player>? OnSecretSantaResult;

	/// <summary>
	/// Only for getting triggered from the server.
	/// </summary>
	public Task SanteeIs(Player santee)
	{
		Console.WriteLine("Received secret santa result: " + santee);
		OnSecretSantaResult?.Invoke(santee);
		return Task.CompletedTask;
	}

	#endregion

}
