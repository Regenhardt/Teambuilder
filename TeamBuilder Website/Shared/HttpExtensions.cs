using System.Net.Http.Json;
using System.Text.Json;

namespace TeamBuilder_Website.Shared;

internal static class HttpExtensions
{
	internal static async Task<T> Deserialize<T>(this Task<HttpResponseMessage> httpCall,
		JsonSerializerOptions jsonSerializerOptions) => (await (await httpCall).Content.ReadFromJsonAsync<T>(jsonSerializerOptions))!;

	internal static async Task<T> PatchAsJsonAsync<T>(this HttpClient http, string route, object data,
		JsonSerializerOptions jsOpts)
	{
		return await http.PatchAsync(route, JsonContent.Create(data))
			.Deserialize<T>(jsOpts);
	}
}
