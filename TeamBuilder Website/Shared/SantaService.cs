using TeamBuilder_Website.Services;
using TeambuilderLibrary.Contracts.Hub;
using TeambuilderLibrary.Models;

namespace TeamBuilder_Website.Shared;

internal class SantaService
{
	private readonly TeambuilderHub hub;
	private readonly ChatService chatService;
	public SantaService(TeambuilderHub hub, ChatService chatService)
	{
		this.hub = hub;
		hub.OnSecretSantaResult += HandleSecretSantaResult;
		
		this.chatService = chatService;
	}

	internal async Task SecretSantaInTeam(Team team) => await hub.StartSecretSantaForTeam(team);

	internal void HandleSecretSantaResult(Player santee) => 
		chatService.HandleMessage(new("[System]", $"You will gift {santee.Name} as a secret santa!"));
}
