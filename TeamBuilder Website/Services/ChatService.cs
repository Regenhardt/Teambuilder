using TeamBuilder_Website.Shared;
using TeambuilderLibrary.Models;

namespace TeamBuilder_Website.Services;

internal class ChatService
{
	public event Action<ChatMessage>? OnMessage;
	private readonly TeambuilderHub hub;
	private readonly AppState state;

	public ChatService(TeambuilderHub hub, AppState state)
	{
		this.hub = hub;
		hub.OnMessage += HandleMessage;
		this.state = state;
	}

	public async Task SendMessage(ChatMessage message)
	{

		if (state.IsLoggedIn)
			message.Sender = state.CurrentPlayer!.Name;

		await hub.SendChatMessage(message);
	}

	internal void HandleMessage(ChatMessage msg) => OnMessage?.Invoke(msg);
}
