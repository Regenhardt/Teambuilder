using Microsoft.JSInterop;

namespace TeamBuilder_Website.Services;

public class LocalStorage(IJSRuntime jSRuntime)
{
	private readonly IJSRuntime jSRuntime = jSRuntime;

	internal async Task Set<T>(string key, T value)
	{
		Console.WriteLine($"Setting {key} to {value}");
		await jSRuntime.InvokeVoidAsync("localStorage.setItem", key, value!);
	}

	internal async Task<T?> Get<T>(string key) where T : class
	{
		Console.WriteLine($"Getting {key}");
		return await jSRuntime.InvokeAsync<T?>("localStorage.getItem", key);
	}

	internal async Task Remove(string key)
	{
		Console.WriteLine($"Removing {key}");
		await jSRuntime.InvokeVoidAsync("localStorage.removeItem", key);
	}

	internal async Task<Guid?> GetGuid(string key)
	{
		var stringData = await Get<string>(key);
		return Guid.TryParse(stringData, out var data)
			? data
			: null;
	}
}
