using TeambuilderLibrary.Models;

namespace TeamBuilder_Website.Services;

internal static class Extensions
{
	internal static void Patch(this Team t, Team newTeam)
	{
		t.Owner = newTeam.Owner;
		t.Members = newTeam.Members;
		t.Name = newTeam.Name;
	}
}
