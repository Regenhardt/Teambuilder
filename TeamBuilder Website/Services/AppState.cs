using TeambuilderLibrary.Contracts;
using TeambuilderLibrary.Models;

namespace TeamBuilder_Website.Services;

public class AppState
{
	private readonly HttpClient http;
	private readonly LocalStorage localStorage;
	private readonly Task initializingTask;
	internal bool Ready => initializingTask.IsCompleted;

	internal async Task WaitForInit()
	{
		if (!Ready)
			await initializingTask;
	}

	internal LiveConnectionState ConnectionState
	{
		get => connectionState;
		set
		{
			connectionState = value;
			OnConnectedStateChanged?.Invoke();
		}
	}
	private LiveConnectionState connectionState;
	public event Action? OnConnectedStateChanged;

	public AppState(HttpClient http, LocalStorage localStorage)
	{
		initializingTask = Task.Run(async () => await RetrieveStoredToken());
		this.http = http;
		this.localStorage = localStorage;
	}

	private async Task RetrieveStoredToken()
	{
		var storedToken = await localStorage.GetGuid(Constants.TokenKey);
		Console.WriteLine($"Retrieval finished, storedToken: {storedToken}");
		if (storedToken.HasValue && storedToken.Value != Guid.Empty)
		{
			await SetToken(storedToken.Value, true);
		}
	}

	#region [ Login ]

	internal Guid Token
	{
		get => Ready ? token : throw new InvalidOperationException("State init not finished");
		private set => token = value;
	}
	private Guid token;

	internal bool IsLoggedIn
	{
		get => isLoggedIn;
		private set
		{
			isLoggedIn = value;
			OnLoginStateChanged?.Invoke();
		}
	}
	private bool isLoggedIn;
	public event Action? OnLoginStateChanged;

	internal async Task SetLoggedIn(Result<Player?> loginResult, bool persistent = false)
	{
		Console.WriteLine($"SetLoggedIn using {loginResult}. Current token is {token}.");
		await SetToken(Guid.TryParse(loginResult.Message, out var newToken) ? newToken : token, persistent);
		CurrentPlayer = loginResult.Data!;
		IsLoggedIn = true;
	}

	internal async Task SetToken(Guid newToken, bool persistent)
	{
		Token = newToken;
		if (persistent)
		{
			await localStorage.Set<Guid?>(Constants.TokenKey, newToken);
		}
		else
		{
			await localStorage.Remove(Constants.TokenKey);
		}
		http.DefaultRequestHeaders.Add(Constants.TokenKey, newToken.ToString());
	}

	internal async Task Logout()
	{
		Console.WriteLine("Logout");
		http.DefaultRequestHeaders.Remove(Constants.TokenKey);
		token = Guid.Empty;
		await localStorage.Remove(Constants.TokenKey);
		CurrentPlayer = null;
		IsLoggedIn = false;
	}

	public Player? CurrentPlayer { get; set; }

	#endregion

	public void ChangePlayerName(string newName)
	{
		Console.WriteLine($"Change player name to {newName}");
		CurrentPlayer!.Name = newName;
		OnLoginStateChanged?.Invoke();
	}

	public void ChangePlayerRank(Rank newRank)
	{
		Console.WriteLine($"Change player rank to {newRank}");
		CurrentPlayer!.Rank = newRank;
		OnLoginStateChanged?.Invoke();
	}
}

internal enum LiveConnectionState
{
	Disconnected,
	Connecting,
	Connected,
	Reconnecting
}
