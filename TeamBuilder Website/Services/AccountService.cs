using System.Net.Http.Json;
using System.Text.Json;
using TeamBuilder_Website.Shared;
using TeambuilderLibrary.Contracts;
using TeambuilderLibrary.Contracts.Account;
using TeambuilderLibrary.Models;

namespace TeamBuilder_Website.Services;

internal class AccountService(HttpClient http, AppState state, TeambuilderHub hub, JsonSerializerOptions jsonOpts) : IAccountService
{
	private readonly HttpClient http = http;
	private readonly AppState state = state;
	private readonly TeambuilderHub hub = hub;
	private readonly JsonSerializerOptions jsonOpts = jsonOpts;

	public Player? CurrentPlayer => state.CurrentPlayer;

	public async Task<Result<string>> RegisterNewUserAsync(RegistrationData userData) => await http.PostAsJsonAsync("/api/Account/register", userData).Deserialize<Result<string>>(jsonOpts).ConfigureAwait(false);

	public async Task Authorize(Guid token)
	{
		Console.WriteLine("Authorizing token");
		var request = new HttpRequestMessage(HttpMethod.Get, "api/Account/login");
		request.Headers.Add(Constants.TokenKey, token.ToString());
		var verify = await http.SendAsync(request).Deserialize<Result<Player?>>(jsonOpts);

		if (verify.Success)
		{
			Console.WriteLine("Token is authorized.");
			await state.SetLoggedIn(verify, true);
		}
	}

	public async Task<Result<Player?>> Login(LoginData loginData)
	{
		var loginResult = await http.PostAsJsonAsync("/api/Account/login", loginData).Deserialize<Result<Player?>>(jsonOpts).ConfigureAwait(false);
		if (loginResult.Success)
		{
			await state.SetLoggedIn(loginResult, loginData.StayLoggedIn);
			var hubMsg = await hub.Login(Guid.Parse(loginResult.Message!));
			Console.WriteLine(hubMsg);
		}
		return loginResult;
	}

	public async Task Logout()
	{
		await http.DeleteAsync("/api/Account/logout").ConfigureAwait(false);
		await state.Logout();
	}

	public async Task LogoutAll()
	{
		if (state.IsLoggedIn)
			await http.DeleteAsync("/api/Account/logout-all").ConfigureAwait(false);
		await state.Logout();
	}

	public Task<Result<string>> Delete(DeletionData deletionData, string token) => throw new NotImplementedException();

	public async Task<Result> ChangePlayerName(string newName)
	{
		if (!state.IsLoggedIn) return Result.Negative("Can't change your name without logging in!");

		return await http.PatchAsJsonAsync<Result>("/api/Account/name", newName, jsonOpts);
	}

	public async Task<Result> ChangePlayerRank(Rank newRank)
	{
		if (!state.IsLoggedIn) return Result.Negative("Can't change your rank without logging in!");

		return await http.PatchAsJsonAsync<Result>("/api/Account/rank", newRank, jsonOpts);
	}

	public short GetCurrentPlayerId() => throw new NotImplementedException();

	public async Task<bool> IsAuthorized(Guid _)
	{
		if (!state.IsLoggedIn)
		{
			await TryLoginFromStoredToken();
		}
		return state.IsLoggedIn;
	}

	private async Task TryLoginFromStoredToken()
	{
		await state.WaitForInit();
		if (state.Token != Guid.Empty)
		{
			await Authorize(state.Token);
		}
	}
}
