using TeamBuilder_Website.Shared;
using TeambuilderLibrary.Contracts.Hub;
using TeambuilderLibrary.Contracts.Team;
using TeambuilderLibrary.Models;

namespace TeamBuilder_Website.Services;

public class TeamService : ITeamService
{
	private readonly TeambuilderHub hub;
	private readonly AlertService alerts;

	public event Action<Team>? OnNewTeam;
	public event Action<string, Team>? OnPatchTeam;
	public event Action<string>? OnDeleteTeam;

	public TeamService(TeambuilderHub hub, AlertService alerts)
	{
		this.hub = hub;
		this.alerts = alerts;
		hub.OnNewTeam += t => OnNewTeam?.Invoke(t);
		hub.OnPatchTeam += (name, team) => OnPatchTeam?.Invoke(name, team);
		hub.OnDeleteTeam += teamName => OnDeleteTeam?.Invoke(teamName);
	}
	public async Task<Result> CreateTeam(string name)
	{
		var result = await hub.CreateTeam(name);
		HandleResult($"Created team {name}", result);
		return result;
	}

	public async Task DeleteTeam(string name)
	{
		var result = await hub.DeleteTeam(name);
		HandleResult($"Deleted team {name}", result);
	}

	/// <summary>
	/// Im Frontend wird hier der Eventstream ausgelöst.
	/// </summary>
	/// <returns></returns>
	public async IAsyncEnumerable<Team> GetAllTeams()
	{
		if (OnNewTeam?.GetInvocationList().Length <= 0)
			throw new InvalidOperationException("Subscribe to event before triggering event stream.");
		await hub.SubscribeToAndReceiveTeamsAsync();
		yield break;
	}

	public async Task<Result> JoinTeam(string teamName)
	{
		var result = await hub.JoinTeam(teamName);
		HandleResult($"Joined team {teamName}", result);
		return result;
	}

	private void HandleResult(string successMessage, Result result)
	{
		if (result.Success)
		{
			alerts.Warning(successMessage);
		}
		else
		{
			alerts.Error(result.Message!);
		}
	}

	public void UpdateTeamName(string oldName, string newName) => throw new NotImplementedException();

	public async Task<Result> LeaveTeam(string teamName)
	{
		var result = await hub.LeaveTeam(teamName);
		HandleResult($"Left team {teamName}", result);
		return result;
	}
}
