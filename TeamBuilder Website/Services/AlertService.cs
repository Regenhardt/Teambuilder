using System.Collections.ObjectModel;
using TeamBuilder_Website.Alerts;

namespace TeamBuilder_Website.Services;

public class AlertService : IAsyncDisposable
{
	private readonly MessageCollection errors = [];
	private readonly MessageCollection warnings = [];

	public AlertService()
	{
		Errors = new ReadOnlyObservableCollection<string>(errors);
		Warnings = new ReadOnlyObservableCollection<string>(warnings);
	}

	public ReadOnlyObservableCollection<string> Errors { get; }

	public ReadOnlyObservableCollection<string> Warnings { get; }

	public void Error(string message)
	{
		Console.WriteLine($"Error: {message}");
		errors.Add(message);
	}

	public void Warning(string message)
	{
		Console.WriteLine($"Warning: {message}");
		warnings.Add(message);
	}

	public async ValueTask DisposeAsync()
	{
		GC.SuppressFinalize(this);
		await errors.DisposeAsync();
		await warnings.DisposeAsync();
	}
}
