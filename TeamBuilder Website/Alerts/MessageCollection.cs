using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace TeamBuilder_Website.Alerts;

public class MessageCollection : ObservableCollection<string>, IAsyncDisposable
{
	public MessageCollection()
	{
		Timer = new Timer(Callback);
		CollectionChanged += MessageCollection_CollectionChanged;
	}

	private Timer Timer { get; }

	private void MessageCollection_CollectionChanged(object? sender, NotifyCollectionChangedEventArgs e) => Timer.Change(5000, 5000);

	private void Callback(object? _)
	{
		if (!this.Any())
		{
			Timer.Change(-1, -1);
			return;
		}

		CollectionChanged -= MessageCollection_CollectionChanged;
		RemoveAt(Count - 1);
		CollectionChanged += MessageCollection_CollectionChanged;
	}

	public ValueTask DisposeAsync()
	{
		GC.SuppressFinalize(this);
		return Timer.DisposeAsync();
	}
}
