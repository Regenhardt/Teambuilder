using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Teambuilder_WebAPI.ActiveAdapters.ApiControllers;
using Teambuilder_WebAPI.Test.ActiveAdapters.ApiControllers;
using TeambuilderLibrary.Models;

namespace Teambuilder_WebAPI.Test.ActiveAdapters.Hubs;

public partial class TeambuilderHubTest
{
	[TestMethod]
	public async Task SubscribeToAndReceiveTeamsAsync_SendsAllTeamsToCaller()
	{
		int anzahl = new Random().Next(30);
		var teams = FakeDataGenerator.GenerateTeams(anzahl);
		teamRepo.Setup(repo => repo.GetAllTeams()).Returns(teams);
		var receiver = RegisterAsCaller();
		receiver.Setup(r => r.ReceiveTeam(It.IsAny<Team>()));

		await hub.SubscribeToAndReceiveTeamsAsync();

		teamRepo.Verify(r => r.GetAllTeams(), Times.Once);
		await FakeDataGenerator.GenerateTeams(anzahl).ForEachAsync(t =>
			receiver.Verify(r => r.ReceiveTeam(It.Is<Team>(team => team.Name == t.Name && team.Owner == t.Owner))));
	}

	[TestMethod]
	public async Task JoinTeam_WithOldToken_ShouldFail()
	{
		var token = Guid.NewGuid();
		SetupLogin(token);
		Assert.IsTrue((await hub.Login(token)).Success);
		accountRepo.Setup(repo => repo.ValidateToken(token)).ReturnsAsync(false);

		await Assert.ThrowsExceptionAsync<SecurityTokenValidationException>(async () => await hub.JoinTeam("Whatever"));
	}

	[TestMethod]
	public async Task JoinTeam_WithInvalidTeam_ShouldReturnNegative()
	{
		var token = Guid.NewGuid();
		var teamId = new Random().Next();
		var teamName = "TestTeam" + teamId;
		var id = (short)new Random().Next(short.MaxValue);
		var player = FakeDataGenerator.GetPlayer(id);

		SetupLogin(token);

		teamRepo.Setup(teams => teams.GetTeamIdByName(teamName)).ReturnsAsync(Result<int>.Negative("Team does not exist"));

		await hub.Login(token);
		var result = await hub.JoinTeam(teamName);

		Assert.IsFalse(result.Success, "Joined non-existing team");
		Assert.IsNotNull(result.Message, "No message received");
		Assert.IsTrue(result!.Message.Contains("exist"), "Should tell the user about non-existing team");
	}

	[TestMethod]
	public async Task JoinTeam_AfterLogout_ShouldFail()
	{
		var token = Guid.NewGuid();
		var teamId = new Random().Next();
		var teamName = "TestTeam" + teamId;

		SetupLogin(token);
		accountRepo.Setup(accounts => accounts.RemoveLoginToken(token))
			.Callback(() => accountRepo.Setup(accounts => accounts.ValidateToken(token)).ReturnsAsync(false))
			.Returns(Task.CompletedTask);
		var loginResult = await hub.Login(token);
		Assert.IsTrue(loginResult.Success);

		await new AccountController(accountService)
		{
			ControllerContext = new ControllerContext
			{
				HttpContext = new DefaultHttpContext()
			}
		}.Logout(token.ToString());

		await Assert.ThrowsExceptionAsync<SecurityTokenValidationException>(
			async () => await hub.JoinTeam(teamName));
	}

	[TestMethod]
	public async Task JoinTeam_WithValidTeam_ShouldReturnPositive_AndAddPlayer_AndPatchTeam()
	{
		// Arrange
		var token = Guid.NewGuid();
		var teamId = new Random().Next();
		var teamName = "TestTeam" + teamId;
		var id = (short)new Random().Next(short.MaxValue);
		var player = FakeDataGenerator.GetPlayer(id);
		var newTeam = new Team
		{
			Members = [player],
			Name = teamName,
			Owner = player
		};

		SetupLogin(token, player, id);

		teamRepo.Setup(teams => teams.GetTeamIdByName(teamName)).ReturnsAsync(Result<int>.Positive(teamId));
		teamRepo.Setup(teams => teams.AddPlayerToTeam(id, teamId)).ReturnsAsync(Result.Positive());
		teamRepo.Setup(teams => teams.GetTeam(teamId)).ReturnsAsync(newTeam);

		var allClients = RegisterAsAll();
		allClients.Setup(all => all.PatchTeam(teamName, It.Is<Team>(t => t.Name == teamName)))
			.Returns(Task.CompletedTask);
		var _ = await hub.Login(token);

		// Act
		var result = await hub.JoinTeam(teamName);

		// Assert
		Assert.IsTrue(result.Success, "Failed to join team");
		teamRepo.Verify(teams => teams.AddPlayerToTeam(id, teamId), Times.Once, "Player not added to team");
		allClients.Verify(
			clients => clients.PatchTeam(teamName,
				It.Is<Team>(t => t.Name == teamName && t.Members.Any(p => p == player))),
			"Clients not notified correctly");
	}

	[TestMethod]
	public async Task LeaveTeam_WithOldToken_ShouldFail()
	{
		var token = Guid.NewGuid();
		SetupLogin(token);
		Assert.IsTrue((await hub.Login(token)).Success);
		accountRepo.Setup(repo => repo.ValidateToken(token)).ReturnsAsync(false);

		await Assert.ThrowsExceptionAsync<SecurityTokenValidationException>(async () => await hub.LeaveTeam("Whatever"));
	}

	[TestMethod]
	public async Task LeaveTeam_WithInvalidTeam_ShouldReturnNegative()
	{
		var token = Guid.NewGuid();
		var teamId = new Random().Next();
		var teamName = "TestTeam" + teamId;
		var id = (short)new Random().Next(short.MaxValue);
		var player = FakeDataGenerator.GetPlayer(id);

		SetupLogin(token, player, id);
		teamRepo.Setup(repo => repo.RemovePlayerFromTeam(id, teamName))
			.ReturnsAsync(Result.Negative("Team doesn't exist"));
		var _ = await hub.Login(token);

		var result = await hub.LeaveTeam(teamName);

		Assert.IsFalse(result.Success, "Left non-existing team");
		Assert.IsNotNull(result.Message, "No message received");
		Assert.IsTrue(result!.Message.Contains("exist"), "Should tell the user about non-existing team");
	}

	[TestMethod]
	public async Task LeaveTeam_WithValidTeam_ShouldReturnPositive_AndRemovePlayer_AndPatchTeam()
	{
		// Arrange
		var token = Guid.NewGuid();
		var teamId = new Random().Next();
		var teamName = "TestTeam" + teamId;
		var id = (short)new Random().Next(short.MaxValue);
		var player = FakeDataGenerator.GetPlayer(id);
		var newTeam = new Team
		{
			Members = [],
			Name = teamName,
			Owner = player
		};

		SetupLogin(token, player, id);

		teamRepo.Setup(teams => teams.RemovePlayerFromTeam(id, teamName)).ReturnsAsync(Result.Positive());
		teamRepo.Setup(teams => teams.GetTeam(teamName)).ReturnsAsync(newTeam);

		var allClients = RegisterAsAll();
		allClients.Setup(all => all.PatchTeam(teamName, It.Is<Team>(t => t.Name == teamName)))
			.Returns(Task.CompletedTask);
		var _ = await hub.Login(token);

		// Act
		var result = await hub.LeaveTeam(teamName);

		// Assert
		Assert.IsTrue(result.Success, "Failed to leave team");
		teamRepo.Verify(teams => teams.RemovePlayerFromTeam(id, teamName), Times.Once, "Player not removed from team");
		allClients.Verify(
			clients => clients.PatchTeam(teamName,
				It.Is<Team>(t => t.Name == teamName && t.Members.All(p => p != player))),
			"Clients not notified correctly");
	}

	[TestMethod]
	public async Task CreateTeam_WithOldToken_ShouldFail()
	{
		var token = Guid.NewGuid();
		SetupLogin(token);
		Assert.IsTrue((await hub.Login(token)).Success);
		accountRepo.Setup(repo => repo.ValidateToken(token)).ReturnsAsync(false);

		await Assert.ThrowsExceptionAsync<SecurityTokenValidationException>(async () => await hub.CreateTeam("Whatever"));
	}

	[TestMethod]
	public async Task CreateTeam_WithExistingTeam_ShouldReturnNegative()
	{
		var token = Guid.NewGuid();
		var teamId = new Random().Next();
		var teamName = "TestTeam" + teamId;
		var id = (short)new Random().Next(short.MaxValue);
		var player = FakeDataGenerator.GetPlayer(id);

		SetupLogin(token, player, id);

		teamRepo.Setup(teams => teams.CreateNewTeam(id, teamName)).ReturnsAsync(Result<Team>.Negative("Team already exists"));
		var _ = await hub.Login(token);

		var result = await hub.CreateTeam(teamName);

		Assert.IsFalse(result.Success, "Created team with already existing name");
		Assert.IsNotNull(result.Message, "No message received");
		Assert.IsTrue(result!.Message.Contains("exists"), "Should tell the user about non-existing team");
	}

	[TestMethod]
	public async Task CreateTeam_AfterLogout_ShouldFail()
	{
		var token = Guid.NewGuid();
		var teamId = new Random().Next();
		var teamName = "TestTeam" + teamId;

		accountRepo.Setup(accounts => accounts.ValidateToken(token)).ReturnsAsync(true);
		accountRepo.Setup(accounts => accounts.GetPlayerFromToken(token)).ReturnsAsync(new Player("PlayerName", Rank.Legend));
		accountRepo.Setup(accounts => accounts.GetPlayerIdFromToken(token)).ReturnsAsync((short)3);
		accountRepo.Setup(accounts => accounts.RemoveLoginToken(token))
			.Callback(() => accountRepo.Setup(accounts => accounts.ValidateToken(token)).ReturnsAsync(false))
			.Returns(Task.CompletedTask);
		groupManager.Setup(man => man.AddToGroupAsync(connectionId, It.IsAny<string>(), It.IsAny<CancellationToken>()))
			.Returns(Task.CompletedTask);
		var loginResult = await hub.Login(token);
		Assert.IsTrue(loginResult.Success);

		await new AccountController(accountService)
		{
			ControllerContext = new ControllerContext
			{
				HttpContext = new DefaultHttpContext()
			}
		}.Logout(token.ToString());

		await Assert.ThrowsExceptionAsync<SecurityTokenValidationException>(
			async () => await hub.CreateTeam(teamName));
	}

	[TestMethod]
	public async Task CreateTeam_WithValidTeam_ShouldReturnPositive_AndAddTeam_AndBroadcastTeam()
	{
		// Arrange
		var token = Guid.NewGuid();
		var teamId = new Random().Next();
		var teamName = "TestTeam" + teamId;
		var id = (short)new Random().Next(short.MaxValue);
		var player = FakeDataGenerator.GetPlayer(id);
		var newTeam = new Team
		{
			Members = [player],
			Name = teamName,
			Owner = player
		};

		SetupLogin(token, player, id);

		teamRepo.Setup(teams => teams.CreateNewTeam(id, teamName)).ReturnsAsync(Result<Team>.Positive(newTeam));
		teamRepo.Setup(teams => teams.GetTeamIdByName(teamName)).ReturnsAsync(Result<int>.Positive(teamId));
		teamRepo.Setup(teams => teams.GetTeam(teamId)).ReturnsAsync(newTeam);

		var allClients = RegisterAsAll();
		allClients.Setup(all => all.PatchTeam(teamName, It.Is<Team>(t => t.Name == teamName)))
			.Returns(Task.CompletedTask);
		var _ = await hub.Login(token);

		// Act
		var result = await hub.CreateTeam(teamName);

		// Assert
		Assert.IsTrue(result.Success, "Failed to create team");
		teamRepo.Verify(teams => teams.CreateNewTeam(id, teamName), Times.Once, "Team was not created");
		allClients.Verify(
			clients => clients.ReceiveTeam(newTeam),
			"Clients not notified correctly");
	}

	[TestMethod]
	public async Task DeleteTeam_AfterLogout_ShouldFail()
	{
		var token = Guid.NewGuid();
		var teamId = new Random().Next();
		var teamName = "TestTeam" + teamId;

		SetupLogin(token);
		accountRepo.Setup(accounts => accounts.RemoveLoginToken(token))
			.Callback(() => accountRepo.Setup(accounts => accounts.ValidateToken(token)).ReturnsAsync(false))
			.Returns(Task.CompletedTask);
		var loginResult = await hub.Login(token);
		Assert.IsTrue(loginResult.Success);

		await new AccountController(accountService)
		{
			ControllerContext = new ControllerContext
			{
				HttpContext = new DefaultHttpContext()
			}
		}.Logout(token.ToString());

		await Assert.ThrowsExceptionAsync<SecurityTokenValidationException>(
			async () => await hub.DeleteTeam(teamName));
	}

	[TestMethod]
	public async Task DeleteTeam_WithValidData_ShouldReturnPositive_AndDeleteTeam_AndBroadcastDeletion()
	{

		// Arrange
		var token = Guid.NewGuid();
		var teamId = new Random().Next();
		var teamName = "TestTeam" + teamId;
		var id = (short)new Random().Next(short.MaxValue);
		var player = FakeDataGenerator.GetPlayer(id);

		SetupLogin(token, player, id);

		teamRepo.Setup(repo => repo.DeleteTeam(id, teamName)).Returns(Task.CompletedTask);

		var allClients = RegisterAsAll();
		allClients.Setup(all => all.PatchTeam(teamName, It.Is<Team>(t => t.Name == teamName)))
			.Returns(Task.CompletedTask);
		var _ = await hub.Login(token);

		// Act
		var result = await hub.DeleteTeam(teamName);

		// Assert
		Assert.IsTrue(result.Success, "Failed to delete team: " + result.Message);
		teamRepo.Verify(teams => teams.DeleteTeam(id, teamName), Times.Once, "Team was not deleted");
	}
}
