using Microsoft.AspNetCore.SignalR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Teambuilder_WebAPI.ActiveAdapters.Hubs;
using Teambuilder_WebAPI.Services;
using TeambuilderLibrary.Contracts;
using TeambuilderLibrary.Contracts.Account;
using TeambuilderLibrary.Contracts.Hub;
using TeambuilderLibrary.Contracts.Team;
using TeambuilderLibrary.Models;

namespace Teambuilder_WebAPI.Test.ActiveAdapters.Hubs;

[TestClass]
public partial class TeambuilderHubTest
{
	private TeambuilderHub hub = default!;
	private Mock<IAccountRepository> accountRepo = default!;
	private IDictionary<object, object?> contextItems = default!;
	private Mock<ITeamRepository> teamRepo = default!;
	private AccountService accountService = default!;
	private Mock<IHubCallerClients<IClientHub>> clientsMock = default!;
	private string connectionId = default!;
	private Mock<IGroupManager> groupManager = default!;

	private Mock<IClientHub> RegisterAsCaller()
	{
		var callerMock = new Mock<IClientHub>();
		clientsMock.SetupGet(c => c.Caller).Returns(callerMock.Object);
		hub.Clients = clientsMock.Object;
		return callerMock;
	}

	private Mock<IClientHub> RegisterAsAll()
	{
		var callerMock = new Mock<IClientHub>();
		clientsMock.SetupGet(c => c.All).Returns(callerMock.Object);
		hub.Clients = clientsMock.Object;
		return callerMock;
	}

	private Mock<IClientHub> RegisterAsOthers()
	{
		var callerMock = new Mock<IClientHub>();
		clientsMock.SetupGet(c => c.Others).Returns(callerMock.Object);
		hub.Clients = clientsMock.Object;
		return callerMock;
	}

	private void SetupLogin(Guid token, Player? player = null, short id = 3)
	{
		accountRepo.Setup(repo => repo.ValidateToken(token)).ReturnsAsync(true);
		accountRepo.Setup(repo => repo.GetPlayerFromToken(token)).ReturnsAsync(player ?? new Player("P1", Rank.Archon));
		accountRepo.Setup(repo => repo.GetPlayerIdFromToken(token)).ReturnsAsync(id);
		groupManager.Setup(man => man.AddToGroupAsync(connectionId, It.IsAny<string>(), It.IsAny<CancellationToken>()))
			.Returns(Task.CompletedTask);
	}

	[TestInitialize]
	public void Setup()
	{
		clientsMock = new Mock<IHubCallerClients<IClientHub>>();
		accountRepo = new Mock<IAccountRepository>(MockBehavior.Strict);
		var cryptoService = new Mock<ICryptoService>();
		teamRepo = new Mock<ITeamRepository>(MockBehavior.Strict);
		var mockContext = new Mock<HubCallerContext>();
		contextItems = new Dictionary<object, object?>();
		mockContext.SetupGet(c => c.Items).Returns(contextItems);
		connectionId = Guid.NewGuid().ToString();
		mockContext.SetupGet(c => c.ConnectionId).Returns(connectionId);

		accountService = new AccountService(accountRepo.Object, cryptoService.Object);

		var teamService = new TeamService(teamRepo.Object, accountService);
		
		hub = new TeambuilderHub(accountService, teamService, null!)
		{
			Context = mockContext.Object,
			Clients = clientsMock.Object
		};
		groupManager = new Mock<IGroupManager>(MockBehavior.Strict);
		hub.Groups = groupManager.Object;
	}

	[TestMethod]
	public async Task Login_WithValidToken_ShouldSucceed_AndSetToken_AndAddPlayerToGroup()
	{
		var token = Guid.NewGuid();
		SetupLogin(token);

		var result = await hub.Login(token);

		Assert.IsTrue(result.Success, "Login fehlgeschlagen, Meldung: " + Environment.NewLine + result.Message);
		Assert.AreEqual(token, contextItems[nameof(token)], "Hub hat beim Login das Token nicht korrekt gesetzt.");
		groupManager.VerifyAll();
	}

	[TestMethod]
	public async Task Login_WithInvalidToken_ShouldReturnedFailedResult()
	{
		var token = Guid.NewGuid();
		accountRepo.Setup(repo => repo.ValidateToken(token)).ReturnsAsync(false);

		var result = await hub.Login(token);

		Assert.IsFalse(result.Success, "Login should have failed but didn't.");
	}
}
