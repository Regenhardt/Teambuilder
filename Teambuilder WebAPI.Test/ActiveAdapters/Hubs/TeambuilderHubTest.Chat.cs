using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TeambuilderLibrary.Models;

namespace Teambuilder_WebAPI.Test.ActiveAdapters.Hubs;

public partial class TeambuilderHubTest
{
	[TestMethod]
	public async Task SendChatMessage_ShouldRelayMessageToOthers()
	{
		// Arrange
		var chatMessage = new ChatMessage("I'm the Sender", "HiImaMessage");
		var othersMock = RegisterAsOthers();
		othersMock.Setup(clients => clients.ReceiveMessage(It.IsAny<ChatMessage>())).Returns(Task.CompletedTask);

		// Act
		await hub.SendChatMessage(chatMessage);

		// Assert
		othersMock.Verify(others => others.ReceiveMessage(chatMessage), Times.Once);
	}

	[TestMethod]
	public async Task SendChatMessage_ShouldNotRelayMessageToSender()
	{
		// Arrange
		var chatMessage = new ChatMessage("I'm the Sender", "HiImaMessage");
		var othersMock = RegisterAsOthers();
		othersMock.Setup(clients => clients.ReceiveMessage(It.IsAny<ChatMessage>())).Returns(Task.CompletedTask);
		var sender = RegisterAsCaller();

		// Act
		await hub.SendChatMessage(chatMessage);

		// Assert
		sender.Verify(caller => caller.ReceiveMessage(chatMessage), Times.Never);
	}
}
