using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Security.Authentication;
using System.Text;
using Teambuilder_WebAPI.ActiveAdapters.ApiControllers;
using Teambuilder_WebAPI.Services;
using TeambuilderLibrary.Contracts;
using TeambuilderLibrary.Contracts.Account;
using TeambuilderLibrary.Models;

namespace Teambuilder_WebAPI.Test.ActiveAdapters.ApiControllers;

[TestClass]
public class AccountControllerTest
{
#pragma warning disable CS8618 // Non-nullable field // Wird im TestInitialize gesetzt
	private Mock<IAccountRepository> accountRepositoryMock;
	private Mock<ICryptoService> cryptoServiceMock;
	private AccountController controller;
#pragma warning restore CS8618

	[TestInitialize]
	public void Setup()
	{
		accountRepositoryMock = new Mock<IAccountRepository>(MockBehavior.Strict);
		cryptoServiceMock = new Mock<ICryptoService>(MockBehavior.Strict);
		cryptoServiceMock.Setup(c => c.Hash(It.IsAny<string>())).Returns(Encoding.UTF8.GetBytes("test"));
		var accountService = new AccountService(accountRepositoryMock.Object, cryptoServiceMock.Object);
		controller = new AccountController(accountService)
		{
			ControllerContext = new ControllerContext
			{
				HttpContext = new DefaultHttpContext()
			}
		};
	}

	[TestMethod]
	public async Task Register_WithMissingData_ShouldReturnNegative()
	{
		var regData = new RegistrationData
		{
			Password = "test",
			Rank = Rank.Ancient,
			Username = null
		};

		var result = await controller.Register(regData);

		Assert.IsFalse(result.Success, "null Username not caught");

		regData.Username = "User";
		regData.Password = null;

		result = await controller.Register(regData);

		Assert.IsFalse(result.Success, "null password not caught");
	}

	[TestMethod]
	public async Task Register_WithInvalidName_ShouldReturnNegative()
	{
		var regData = new RegistrationData
		{
			Username = new string('h', 21),
			Password = "test",
			Rank = Rank.Ancient
		};

		var result = await controller.Register(regData);

		Assert.IsFalse(result.Success, "Too long name not caught");
		Assert.IsTrue(result.Message!.Contains("Username"), "No Username in message");
	}

	[TestMethod]
	public async Task Register_WithInvalidPassword_ShouldReturnNegative()
	{
		var regData = new RegistrationData
		{
			Username = "ImaUser",
			Password = "1",
			Rank = Rank.Ancient
		};

		var result = await controller.Register(regData);

		Assert.IsFalse(result.Success, "Too short password not caught");
		Assert.IsTrue(result.Message!.Contains("Password"), "No password in message");
	}

	[TestMethod]
	public async Task Register_WithValidData_ShouldReturnPositive()
	{
		var regData = new RegistrationData
		{
			Password = "test",
			Rank = Rank.Ancient,
			Username = "ImaUser"
		};
		accountRepositoryMock.Setup(repo => repo.AccountExists("ImaUser"))
			.Returns(Task.FromResult(false));
		accountRepositoryMock
			.Setup(repo => repo.RegisterNewAccountAsync(regData.Username, It.IsAny<byte[]>(), regData.Rank))
			.Returns(Task.CompletedTask);

		var result = await controller.Register(regData);

		Assert.IsTrue(result.Success, "Registration failed");
	}

	[TestMethod]
	public async Task Register_WithExistingUser_ShouldReturnNegative()
	{
		var regData = new RegistrationData
		{
			Password = "test",
			Rank = Rank.Ancient,
			Username = "ImaUser"
		};
		accountRepositoryMock.Setup(repo => repo.AccountExists("ImaUser"))
			.Returns(Task.FromResult(true));

		var result = await controller.Register(regData);

		Assert.IsFalse(result.Success, "User registered with existing name");
	}

	[TestMethod]
	public async Task Login_WithInvalidUser_ShouldReturnNegative()
	{
		var loginData = new LoginData
		{
			Password = "test",
			StayLoggedIn = false,
			Username = "ImaUser"
		};
		accountRepositoryMock.Setup(repo => repo.AccountExists(loginData.Username)).ReturnsAsync(false);

		var result = await controller.Login(loginData);

		Assert.IsFalse(result.Success, "Nicht vorhandener User eingeloggt");
		Assert.IsNull(result.Data, "Spielerdaten trotz fehlgeschlagenem Login erhalten");
	}

	[TestMethod]
	public async Task Login_WithInvalidPassword_ShouldReturnNegative()
	{
		var loginData = new LoginData
		{
			Password = "test",
			StayLoggedIn = false,
			Username = "ImaUser"
		};
		accountRepositoryMock.Setup(repo => repo.AccountExists(loginData.Username)).ReturnsAsync(true);
		const string msg = "Wrong password";
		accountRepositoryMock.Setup(repo => repo.GetPlayerFromCredentials(loginData.Username, It.IsAny<byte[]>()))
			.Throws(new InvalidCredentialException(msg));

		var result = await controller.Login(loginData);

		Assert.IsFalse(result.Success, "Trotz falschem Passwort eingeloggt");
		Assert.IsNull(result.Data, "Spielerdaten trotz fehlgeschlagenem Login erhalten");
		Assert.AreEqual(msg, result.Message, "Fehlermeldung?");
	}

	[TestMethod]
	public async Task Login_WithValidCredentials_ShouldReturnPlayerAndToken()
	{
		var loginData = new LoginData
		{
			Password = "Test",
			StayLoggedIn = false,
			Username = "ImaUser"
		};
		accountRepositoryMock.Setup(repo => repo.AccountExists(It.IsAny<string>())).ReturnsAsync(true);
		var player = new Player { Name = loginData.Username, Rank = Rank.Ancient };
		accountRepositoryMock.Setup(repo => repo.GetPlayerFromCredentials(It.IsAny<string>(), It.IsAny<byte[]>()))
			.ReturnsAsync(player);
		var guid = Guid.NewGuid();
		accountRepositoryMock.Setup(repo => repo.GetNewTokenForPlayer(It.IsAny<string>()))
			.ReturnsAsync(guid);

		var result = await controller.Login(loginData);

		Assert.IsTrue(result.Success);
		Assert.AreEqual(player, result.Data, "Falscher Spieler geliefert");
		Assert.AreEqual(guid.ToString(), controller.Response.Headers[Constants.TokenKey].ToString());
	}

	[TestMethod]
	public async Task Validate_WithInvalidToken_ShouldReturnNegative()
	{
		var token = Guid.NewGuid();
		accountRepositoryMock.Setup(repo => repo.ValidateToken(token)).ReturnsAsync(false);

		var result = await controller.Validate(token);

		Assert.IsFalse(result.Success);
		Assert.IsNull(result.Data, "Sent player data despite invalid token");
	}

	[TestMethod]
	public async Task Validate_WithValidToken_ShouldReturnPositive()
	{
		var token = Guid.NewGuid();
		accountRepositoryMock.Setup(repo => repo.ValidateToken(token)).ReturnsAsync(true);
		var player = new Player { Name = "ImaUser", Rank = Rank.Ancient };
		const short id = 42;
		accountRepositoryMock.Setup(repo => repo.GetPlayerFromToken(token))
			.ReturnsAsync(player);
		accountRepositoryMock.Setup(repo => repo.GetPlayerIdFromToken(token)).ReturnsAsync(id);

		var result = await controller.Validate(token);

		Assert.IsTrue(result.Success, "Token wasn't validated");
		Assert.AreEqual(player, result.Data, "Wrong player returned");
	}

	[TestMethod]
	public async Task Logout_WithoutToken_JustFinishes() =>
		// No Mocks because nothing should be called
		await controller.Logout(null);

	[TestMethod]
	public async Task LogoutAll_WithoutToken_JustFinishes() =>
		// No Mocks because nothing should be called
		await controller.LogoutAll(null);

	[TestMethod]
	public async Task Logout_WithToken_InvalidatesToken()
	{
		var token = Guid.NewGuid();

		accountRepositoryMock.Setup(accounts => accounts.ValidateToken(token)).ReturnsAsync(true);
		accountRepositoryMock.Setup(accounts => accounts.GetPlayerFromToken(token)).ReturnsAsync(new Player("PlayerName", Rank.Legend));
		accountRepositoryMock.Setup(accounts => accounts.GetPlayerIdFromToken(token)).ReturnsAsync((short)3);
		accountRepositoryMock.Setup(repo => repo.RemoveLoginToken(token)).Returns(Task.CompletedTask);

		await controller.Logout(token.ToString());

		accountRepositoryMock.Verify(repo => repo.RemoveLoginToken(token), Times.Once);
	}
	[TestMethod]
	public async Task LogoutAll_WithToken_InvalidatesAllToken()
	{
		var token = Guid.NewGuid();

		accountRepositoryMock.Setup(accounts => accounts.ValidateToken(token)).ReturnsAsync(true);
		accountRepositoryMock.Setup(accounts => accounts.GetPlayerFromToken(token)).ReturnsAsync(new Player("PlayerName", Rank.Legend));
		accountRepositoryMock.Setup(accounts => accounts.GetPlayerIdFromToken(token)).ReturnsAsync((short)3);
		accountRepositoryMock.Setup(repo => repo.RemoveAllTokensFor(token)).Returns(Task.CompletedTask);

		await controller.LogoutAll(token.ToString());

		accountRepositoryMock.Verify(repo => repo.RemoveAllTokensFor(token), Times.Once);
	}

	[TestMethod]
	public async Task Delete_WithoutToken_ShouldReturnNegative()
	{
		var result = await controller.Delete(null, null);

		Assert.IsFalse(result.Success);
		Assert.IsTrue(result.Message!.ToLower().Contains("logged in"), "Wrong message: " + result.Message);
	}

	[TestMethod]
	public async Task Delete_WithoutData_ShouldReturnNegative()
	{
		var result = await controller.Delete(Guid.NewGuid().ToString(), null);

		Assert.IsFalse(result.Success);
	}

	[TestMethod]
	public async Task Delete_WithoutUser_ShouldReturnNegative()
	{
		var deletionData = new DeletionData();
		var result = await controller.Delete(Guid.NewGuid().ToString(), deletionData);

		Assert.IsFalse(result.Success);
		Assert.IsTrue(result.Message!.ToLower().Contains("user"), "Wrong message: " + result.Message);
	}

	[TestMethod]
	public async Task Delete_WithoutPassword_ShouldReturnNegative()
	{
		var deletionData = new DeletionData
		{
			Username = "ImaUser"
		};
		var result = await controller.Delete(Guid.NewGuid().ToString(), deletionData);

		Assert.IsFalse(result.Success);
		Assert.IsTrue(result.Message!.ToLower().Contains("password"), "Wrong message: " + result.Message);
	}

	[TestMethod]
	public async Task Delete_WithWrongLogin_ShouldReturnNegative()
	{
		var token = Guid.NewGuid();
		var deletionData = new DeletionData
		{
			Username = "ImaUser",
			Password = "test"
		};
		accountRepositoryMock
			.Setup(repo => repo.ValidateActiveUserData(deletionData.Username, It.IsAny<byte[]>(), token))
			.ReturnsAsync(false);
		var result = await controller.Delete(token.ToString(), deletionData);

		Assert.IsFalse(result.Success);
		Assert.IsTrue(result.Message!.ToLower().Contains("logged"), "Wrong message: " + result.Message);
	}

	[TestMethod]
	public async Task Delete_WithCorrectData_ShouldDeleteUserAfterValidation()
	{
		var token = Guid.NewGuid();
		var deletionData = new DeletionData
		{
			Username = "ImaUser",
			Password = "test"
		};
		var sequence = new MockSequence(); // Validate before Delete
		accountRepositoryMock
			.InSequence(sequence)
			.Setup(repo => repo.ValidateActiveUserData(deletionData.Username, It.IsAny<byte[]>(), token))
			.ReturnsAsync(true);
		accountRepositoryMock
			.InSequence(sequence)
			.Setup(repo => repo.DeleteAccount(deletionData.Username)).Returns(Task.CompletedTask);

		var result = await controller.Delete(token.ToString(), deletionData);

		Assert.IsTrue(result.Success, "Failed with message: " + result.Message);
		accountRepositoryMock.Verify(
			repo => repo.ValidateActiveUserData(deletionData.Username, It.IsAny<byte[]>(), token), Times.Once,
			"Deleted user without validation.");
		accountRepositoryMock.Verify(repo => repo.DeleteAccount(deletionData.Username), Times.Once);
	}

	[TestMethod]
	public async Task ChangeName_WithoutToken_ShouldReturnNegative()
	{
		var token = Guid.NewGuid();

		accountRepositoryMock.Setup(repo => repo.ValidateToken(token)).ReturnsAsync(false);

		var result = await controller.ChangeName(token, "NewName");

		Assert.IsFalse(result.Success);
	}

	[TestMethod]
	public async Task ChangeName_WithInvalidToken_ShouldReturnNegative()
	{
		var token = Guid.NewGuid();

		accountRepositoryMock.Setup(repo => repo.ValidateToken(token)).ReturnsAsync(false);

		var result = await controller.ChangeName(token, "NewName");

		Assert.IsFalse(result.Success);
	}

	[TestMethod]
	public async Task ChangeName_WithExistingName_ShouldReturnNegative()
	{
		var token = Guid.NewGuid();
		var newName = "NewName";
		accountRepositoryMock.Setup(repo => repo.ValidateToken(token)).ReturnsAsync(true);
		accountRepositoryMock.Setup(repo => repo.GetPlayerFromToken(token)).ReturnsAsync(new Player());
		accountRepositoryMock.Setup(repo => repo.GetPlayerIdFromToken(token)).ReturnsAsync((short)Guid.NewGuid().GetHashCode());
		accountRepositoryMock.Setup(repo => repo.AccountExists(newName)).ReturnsAsync(true);

		var result = await controller.ChangeName(token, newName);

		Assert.IsFalse(result.Success);
	}

	[TestMethod]
	public async Task ChangeName_WithValidToken_ShouldChangeNameAfterValidation()
	{
		var token = Guid.NewGuid();

		var sequence = new MockSequence();
		accountRepositoryMock.InSequence(sequence)
			.Setup(repo => repo.ValidateToken(token)).ReturnsAsync(true);
		var id = (short)Guid.NewGuid().GetHashCode();
		var newName = "NewName";
		var player = new Player
		{
			Name = "OldName",
			Rank = Rank.Ancient
		};
		accountRepositoryMock.InSequence(sequence)
			.Setup(repo => repo.GetPlayerFromToken(token)).ReturnsAsync(player);
		accountRepositoryMock.InSequence(sequence)
			.Setup(repo => repo.GetPlayerIdFromToken(token))
			.ReturnsAsync(id);
		accountRepositoryMock.InSequence(sequence)
			.Setup(repo => repo.AccountExists(newName)).ReturnsAsync(false);
		accountRepositoryMock.InSequence(sequence)
			.Setup(repo => repo.ChangeName(id, newName)).Returns(Task.CompletedTask);


		var result = await controller.ChangeName(token, newName);

		Assert.IsTrue(result.Success);
		accountRepositoryMock.Verify(repo => repo.ChangeName(id, newName), Times.Once);
	}

	[TestMethod]
	public async Task ChangeRank_WithoutToken_ShouldReturnNegative()
	{
		var token = Guid.NewGuid();

		accountRepositoryMock.Setup(repo => repo.ValidateToken(token)).ReturnsAsync(false);

		var result = await controller.ChangeRank(token, Rank.Archon);

		Assert.IsFalse(result.Success, "Rang ohne Token geändert");
	}

	[TestMethod]
	public async Task ChangeRank_WithInvalidToken_ShouldReturnNegative()
	{
		var token = Guid.NewGuid();

		accountRepositoryMock.Setup(repo => repo.ValidateToken(token)).ReturnsAsync(false);

		var result = await controller.ChangeRank(token, Rank.Archon);

		Assert.IsFalse(result.Success, "Rank trotz falschem Token geändert");
	}

	[TestMethod]
	public async Task ChangeRank_WithValidToken_ShouldChangeRankAfterValidation()
	{
		var token = Guid.NewGuid();

		var sequence = new MockSequence();
		accountRepositoryMock.InSequence(sequence)
			.Setup(repo => repo.ValidateToken(token)).ReturnsAsync(true);
		var id = (short)Guid.NewGuid().GetHashCode();
		var newRank = Rank.Crusader;
		var player = new Player
		{
			Name = "OldName",
			Rank = Rank.Ancient
		};
		accountRepositoryMock.InSequence(sequence)
			.Setup(repo => repo.GetPlayerFromToken(token)).ReturnsAsync(player);
		accountRepositoryMock.InSequence(sequence)
			.Setup(repo => repo.GetPlayerIdFromToken(token))
			.ReturnsAsync(id);
		accountRepositoryMock.InSequence(sequence)
			.Setup(repo => repo.ChangeRank(id, newRank)).Returns(Task.CompletedTask);


		var result = await controller.ChangeRank(token, Rank.Crusader);

		Assert.IsTrue(result.Success, result.Message);
		accountRepositoryMock.Verify(repo => repo.ChangeRank(id, newRank), Times.Once);
	}
}
