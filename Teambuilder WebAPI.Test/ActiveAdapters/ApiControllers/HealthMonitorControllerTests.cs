using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Teambuilder_WebAPI.ActiveAdapters.ApiControllers;
using TeambuilderLibrary.Contracts.Account;

namespace Teambuilder_WebAPI.Test.ActiveAdapters.ApiControllers;

[TestClass]
public class HealthMonitorControllerTests
{
	private HealthMonitorController controller = null!;
	private Mock<IAccountRepository> accountRepo = null!;

	[TestInitialize]
	public void Setup()
	{
		var providerMock = new Mock<IServiceProvider>(MockBehavior.Strict);
		accountRepo = new Mock<IAccountRepository>(MockBehavior.Strict);
		providerMock.Setup(provider => provider.GetService(typeof(IAccountRepository))).Returns(accountRepo.Object);
		controller = new HealthMonitorController(providerMock.Object);
	}
	[TestMethod]
	public void Get_ShouldWork() => Assert.IsNotNull(controller.Get());

	[TestMethod]
	public async Task DatabaseAsync_WithDatabase_ShouldReturnOk()
	{
		accountRepo.Setup(repo => repo.AccountExists(It.IsAny<string>())).ReturnsAsync(false);
		Assert.AreEqual("OK", await controller.DatabaseAsync());
	}

	[TestMethod]
	public async Task DatabaseAsync_WithoutDatabase_ShouldReturnMessage()
	{
		var message = "ImaDbException";
		accountRepo.Setup(repo => repo.AccountExists(It.IsAny<string>()))
			.Throws(new Exception(message));
		var result = await controller.DatabaseAsync();
		Assert.IsTrue(result.Contains(message), "Was: " + result);
	}
}
