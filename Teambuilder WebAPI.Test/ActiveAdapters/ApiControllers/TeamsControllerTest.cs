using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Teambuilder_WebAPI.ActiveAdapters.ApiControllers;
using Teambuilder_WebAPI.Services;
using TeambuilderLibrary.Contracts;
using TeambuilderLibrary.Contracts.Account;
using TeambuilderLibrary.Contracts.Team;

namespace Teambuilder_WebAPI.Test.ActiveAdapters.ApiControllers;

[TestClass]
public class TeamsControllerTest
{
#pragma warning disable CS8618 // Nullable // -> TestInitialize
	private TeamsController controller;
	private Mock<ITeamRepository> teamsRepositoryMock;
#pragma warning restore CS8618 // 

	[TestInitialize]
	public void Setup()
	{
		teamsRepositoryMock = new Mock<ITeamRepository>();
		var accountRepositoryMock = new Mock<IAccountRepository>();
		var cryptoMock = new Mock<ICryptoService>();

		var accountService = new AccountService(accountRepositoryMock.Object, cryptoMock.Object);
		var teamService = new TeamService(teamsRepositoryMock.Object, accountService);

		controller = new TeamsController(teamService);
	}

	[DataTestMethod]
	[DataRow(0)]
	[DataRow(5)]
	public async Task GetTeamsAsync_ShouldReturnAllTeams(int anzahlTeams)
	{
		teamsRepositoryMock.Setup(repo => repo.GetAllTeams()).Returns(FakeDataGenerator.GenerateTeams(anzahlTeams));

		var result = await controller.GetTeamsAsync().ToListAsync();

		Assert.AreEqual(anzahlTeams, result.Count);
		Assert.IsTrue(result.TrueForAll(team =>
				team.Owner != null && team.Members.Count > 0 && !string.IsNullOrWhiteSpace(team.Name)),
			"Teams falsch ausgegeben");
	}
}
