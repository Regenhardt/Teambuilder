using TeambuilderLibrary.Models;

namespace Teambuilder_WebAPI.Test.ActiveAdapters.ApiControllers;

public class FakeDataGenerator
{
	/// <summary>
	/// Completely deterministic - 5 teams will always generate the same 5 teams in the same order.
	/// </summary>
	/// <param name="anzahl"></param>
	/// <returns></returns>
	internal static async IAsyncEnumerable<Team> GenerateTeams(int anzahl)
	{
		for (int i = 0; i < anzahl; i++)
		{
			yield return new Team
			{
				Members = GeneratePlayers(anzahl + 1 - i).ToList(),
				Name = $"Team#{i}",
				Owner = GetPlayer(anzahl + 1 - i)
			};
		}

		await Task.CompletedTask;
	}

	private static IEnumerable<Player> GeneratePlayers(int anzahl)
	{
		for (var i = 0; i < anzahl; i++)
		{
			yield return GetPlayer(i);
		}
	}

	internal static Player GetPlayer(int seed)
	{
		return new Player
		{
			Name = $"Player#{seed}",
			Rank = (Rank)(seed % Enum.GetValues<Rank>().Cast<int>().Max())
		};
	}
}
