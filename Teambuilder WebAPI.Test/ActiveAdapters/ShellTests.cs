using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Text;
using Teambuilder_WebAPI.ActiveAdapters;
using Teambuilder_WebAPI.Services;
using TeambuilderLibrary.Contracts;
using TeambuilderLibrary.Contracts.Account;
using TeambuilderLibrary.Contracts.Team;
using TeambuilderLibrary.Models;

namespace Teambuilder_WebAPI.Test.ActiveAdapters;

[TestClass]
public class ShellTests
{
	private ShellWrapper shell = null!;
	private Mock<IServiceScope> scopeMock = null!;
	private static readonly Team superTeam = new()
	{
		Members = [],
		Name = "Superteam",
		Owner = new Player("Supercoach", Rank.Legend)
	};

	private static async IAsyncEnumerable<Team> GetTeam()
	{
		yield return superTeam;
		await Task.CompletedTask;
	}

	[TestInitialize]
	public void Setup()
	{
		var teamRepo = new Mock<ITeamRepository>();
		teamRepo.Setup(repo => repo.GetAllTeams()).Returns(GetTeam());
		var accountRepo = new Mock<IAccountRepository>();
		var cryptoService = new Mock<ICryptoService>();

		var accountService = new AccountService(accountRepo.Object, cryptoService.Object);
		var teamService = new TeamService(teamRepo.Object, accountService);

		var provider = new Mock<IServiceProvider>();
		scopeMock = new Mock<IServiceScope>();
		scopeMock.SetupGet(scope => scope.ServiceProvider).Returns(provider.Object);
		var scopeFactory = new Mock<IServiceScopeFactory>();
		scopeFactory.Setup(factory => factory.CreateScope()).Returns(scopeMock.Object);
		provider.Setup(sp => sp.GetService(typeof(IServiceScopeFactory))).Returns(scopeFactory.Object);
		provider.Setup(sp => sp.GetService(typeof(ITeamService))).Returns(teamService);

		shell = new ShellWrapper(provider.Object);
	}

	[TestMethod]
	public void Dispose_ShouldDisposeScope()
	{
		shell.Dispose();

		scopeMock.Verify(scope => scope.Dispose(), Times.Once);
	}

	[TestMethod]
	public async Task Execute_ShouldReturnValidTask()
	{
		DeactivateConsole();
		var task = shell.Execute(CancellationToken.None);
		await shell.StopAsync(new CancellationToken(true));
		Assert.IsNotNull(task);
	}

	[TestMethod]
	public async Task Loop_WithCancelledToken_ShouldFinish()
	{
		var task = shell.StartAsync(new CancellationToken(true));
		await Task.Delay(50);
		Assert.IsTrue(task.IsCompleted);
	}

	[TestMethod]
	public async Task Loop_ShouldWriteToConsole_AndShutDown()
	{
		var sb = new StringBuilder();
		await using var writer = new StringWriter(sb);
		Console.SetOut(writer);
		Console.SetIn(new StringReader("Q\n"));
		var src = new CancellationTokenSource();
		var task = shell.Execute(src.Token);
		await Task.Delay(200, CancellationToken.None);
		src.Cancel();
		writer.Close();
		await writer.FlushAsync();

		Assert.IsTrue(sb.ToString().Contains("Shutting down"), "Shell didn't shut down.");
	}

	[TestMethod]
	public async Task Loop_ShouldWriteDateToConsole_AndShutDown()
	{
		var sb = new StringBuilder();
		await using var writer = new StringWriter(sb);
		Console.SetOut(writer);
		Console.SetIn(new StringReader("D\n"));
		var src = new CancellationTokenSource();
		var task = shell.Execute(src.Token);
		await Task.Delay(200, CancellationToken.None);
		src.Cancel();
		writer.Close();
		await writer.FlushAsync();

		Assert.IsTrue(sb.ToString().Contains(DateTime.Now.ToString("yyyy-MM-dd")),
			"Expected date not found, output was:" + Environment.NewLine + sb);
		Assert.IsTrue(sb.ToString().Contains("Shutting down"), "Shell didn't shut down. Output was:" + Environment.NewLine + sb);
	}

	[TestMethod]
	public async Task Loop_ShouldWriteTeamToConsole_AndShutDown()
	{
		var sb = new StringBuilder();
		await using var writer = new StringWriter(sb);
		Console.SetOut(writer);
		Console.SetIn(new StringReader("T\n"));
		var src = new CancellationTokenSource();
		var task = shell.Execute(src.Token);
		await Task.Delay(200, CancellationToken.None);
		src.Cancel();
		writer.Close();
		await writer.FlushAsync();

		Assert.IsTrue(sb.ToString().Contains(superTeam.ToString()),
			"Expected team not found, output was:" + Environment.NewLine + sb);
		Assert.IsTrue(sb.ToString().Contains("Shutting down"), "Shell didn't shut down. Output was:" + Environment.NewLine + sb);
	}

	private static void DeactivateConsole()
	{
		Console.In.Close();
		Console.Out.Close();
		Console.Out.Dispose();
		Console.In.Dispose();
	}
}

internal class ShellWrapper(IServiceProvider sp) : Shell(sp)
{
	public Task Execute(CancellationToken token) => ExecuteAsync(token);
}
