using Microsoft.AspNetCore.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ComponentModel.DataAnnotations;
using System.Text.Json;
using Teambuilder_WebAPI.Services;
using TeambuilderLibrary.Models;

namespace Teambuilder_WebAPI.Test.Services;

[TestClass]
public class ExceptionHandlerTest
{
	private const string msg = "I'm an exception message";

	[TestMethod]
	public async Task InvokeAsync_WhenDelegateThrows_ShouldReturnResult()
	{
		var context = new DefaultHttpContext();
		context.Response.Body = new MemoryStream();

		await new ExceptionHandler().InvokeAsync(context, _ => throw new ValidationException(msg));

		Assert.AreEqual(200, context.Response.StatusCode);
		context.Response.Body.Seek(0, SeekOrigin.Begin);
		var data = await new StreamReader(context.Response.Body).ReadToEndAsync();
		var result = JsonSerializer.Deserialize<Result<string>>(data);
		Assert.IsTrue(result.Message!.Contains(msg), "Message not relayed, was: " + result.Message);
	}

	[TestMethod]
	public async Task InvokeAsync_WhenDelegateWorks_ShouldRelayResult()
	{
		var context = new DefaultHttpContext();

		await new ExceptionHandler().InvokeAsync(context, r => { r.Response.Body = new MemoryStream(); return Task.CompletedTask; });

		Assert.AreEqual(200, context.Response.StatusCode);
		Assert.IsTrue(context.Response.Body is MemoryStream { Length: 0 }, "Body not empty MemoryStream, was: " + context.Response.Body);
		;
	}
}
