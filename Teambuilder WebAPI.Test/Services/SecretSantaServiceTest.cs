using Microsoft.VisualStudio.TestTools.UnitTesting;
using Teambuilder_WebAPI.Services;
using TeambuilderLibrary.Models;

namespace Teambuilder_WebAPI.Test.Services;

[TestClass]
public class SecretSantaServiceTest
{
	[TestMethod]
	public void SecretSantaWithTwoPeople()
	{
		var members = new List<Player>
			{
				new Player { Name = "P1"},
				new Player { Name = "P2"}
			};

		Team testTeam = new()
		{
			Name = "TestTeam",
			Owner = members[0],
			Members = members
		};
		var sut = new SecretSantaService();

		var santaResult = sut.GetSecretSantas(testTeam);

		Assert.IsNotNull(santaResult);
		Assert.AreEqual(santaResult[members[0]], members[1]);
		Assert.AreEqual(santaResult[members[1]], members[0]);
	}

	[TestMethod]
	public void SecretSantaWithMultiplePeople()
	{
		var members = new List<Player>
			{
				new Player { Name = "P1"},
				new Player { Name = "P2"},
				new Player { Name = "P3"},
				new Player { Name = "P4"}
			};

		Team testTeam = new()
		{
			Name = "TestTeam",
			Owner = members[0],
			Members = members
		};
		var sut = new SecretSantaService();

		var santaResult = sut.GetSecretSantas(testTeam);

		foreach (var player in members)
		{
			Assert.AreNotEqual(santaResult[player], player);
		}
	}
}
