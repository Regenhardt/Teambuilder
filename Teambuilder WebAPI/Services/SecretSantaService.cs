using TeambuilderLibrary.Contracts;
using TeambuilderLibrary.Models;

namespace Teambuilder_WebAPI.Services;

public class SecretSantaService : ISecretSantaService
{
	private readonly Random random = new();
	public Dictionary<Player, Player> GetSecretSantas(Team team)
	{
		if (team.Members.Count < 2)
		{
			return [];
		}

		var poolOfSantees = team.Members.ToList();
		var secretSantas = new Dictionary<Player, Player>();

		foreach (var santa in team.Members)
		{
			var santee = GetSantee(santa, poolOfSantees);
			secretSantas.Add(santa, santee);
			poolOfSantees.Remove(santee);
		}

		return secretSantas;
	}

	private Player GetSantee(Player santa, List<Player> poolOfSantees)
	{
		while (true)
		{
			var potentialSanta = poolOfSantees[random.Next(poolOfSantees.Count)];
			if (potentialSanta != santa) return potentialSanta;
		}
		throw new Exception("Kein Secret santa gefunden");
	}
}
