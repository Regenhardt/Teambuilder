using TeambuilderLibrary.Contracts.Account;
using TeambuilderLibrary.Contracts.Team;
using TeambuilderLibrary.Models;

namespace Teambuilder_WebAPI.Services;

public class TeamService(ITeamRepository teamRepository, IAccountService accountService) : ITeamService
{
	private readonly ITeamRepository teamRepository = teamRepository;
	private readonly IAccountService accountService = accountService;

	public event Action<Team>? OnNewTeam;
	public event Action<string, Team>? OnPatchTeam;
	public event Action<string>? OnDeleteTeam;

	public async IAsyncEnumerable<Team> GetAllTeams()
	{
		await foreach (var team in teamRepository.GetAllTeams())
		{
			yield return team;
		}
	}

	public Team GetTeam(int id) => throw new NotImplementedException();

	public async Task<Result> CreateTeam(string name)
	{
		var playerId = accountService.GetCurrentPlayerId();
		var result = await teamRepository.CreateNewTeam(playerId, name);
		if (result.Success)
		{
			OnNewTeam?.Invoke(result.Data);
			return Result.Positive();
		}

		return Result.Negative(result.Message!);
	}

	public async Task DeleteTeam(string name)
	{
		var playerId = accountService.GetCurrentPlayerId();
		await teamRepository.DeleteTeam(playerId, name);
		OnDeleteTeam?.Invoke(name);
	}

	public void UpdateTeamName(string oldName, string newName) => throw new NotImplementedException();

	public async Task<Result> JoinTeam(string teamName)
	{
		var playerId = accountService.GetCurrentPlayerId();
		// Let repo join player to team
		var getTeamId = await teamRepository.GetTeamIdByName(teamName);
		if (!getTeamId.Success)
		{
			return Result.Negative(getTeamId.Message!);
		}

		var result = await teamRepository.AddPlayerToTeam(playerId, getTeamId.Data);
		if (result.Success)
		{
			OnPatchTeam?.Invoke(teamName, await teamRepository.GetTeam(getTeamId.Data));
		}

		return result;
	}

	public async Task<Result> LeaveTeam(string teamName)
	{
		var playerId = accountService.GetCurrentPlayerId();
		// Let repo remove player from team
		var result = await teamRepository.RemovePlayerFromTeam(playerId, teamName);
		if (result.Success)
		{
			var team = await teamRepository.GetTeam(teamName);
			Console.WriteLine($"Team now: {team}");
			OnPatchTeam?.Invoke(teamName, team);
		}

		return result;
	}
}
