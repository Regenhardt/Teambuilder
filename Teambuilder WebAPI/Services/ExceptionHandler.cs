using System.Text.Json;
using TeambuilderLibrary.Models;

namespace Teambuilder_WebAPI.Services;

public class ExceptionHandler : IMiddleware
{
	public async Task InvokeAsync(HttpContext context, RequestDelegate next)
	{
		try
		{
			await next(context);
		}
		catch (Exception e)
		{
			var result = JsonSerializer.Serialize(Result.Negative(e.ToString()));
			context.Response.StatusCode = StatusCodes.Status200OK;
			context.Response.ContentType = "application/json";
			await context.Response.WriteAsync(result);
		}
	}
}
