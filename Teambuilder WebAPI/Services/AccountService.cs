using Microsoft.IdentityModel.Tokens;
using System.Security.Authentication;
using TeambuilderLibrary.Contracts;
using TeambuilderLibrary.Contracts.Account;
using TeambuilderLibrary.Models;

namespace Teambuilder_WebAPI.Services;

public class AccountService(IAccountRepository accountRepository, ICryptoService cryptoService) : IAccountService
{
	private readonly IAccountRepository accountRepository = accountRepository;
	private readonly ICryptoService cryptoService = cryptoService;
	private Player? currentPlayer;
	private short? currentPlayerId;
	private Guid? currentToken;

	public Player? CurrentPlayer => currentPlayer;

	public async Task Authorize(Guid token)
	{
		if (!await IsAuthorized(token))
			ThrowForAuth();

		currentPlayer = await accountRepository.GetPlayerFromToken(token);
		currentPlayerId = await accountRepository.GetPlayerIdFromToken(token);
		currentToken = token;
	}

	private static dynamic ThrowForAuth() => throw new SecurityTokenValidationException();

	public async Task<Result<string>> RegisterNewUserAsync(RegistrationData userData)
	{
		if (!UserDataValidator.IsUsernameValid(userData.Username, out var message))
		{
			return Result<string>.Negative(message);
		}

		if (!UserDataValidator.IsPasswordValid(userData.Password, out message))
		{
			return Result<string>.Negative(message);
		}


		if (await accountRepository.AccountExists(userData.Username!).ConfigureAwait(false))
		{
			message = "Spielername ist vergeben";
			return Result<string>.Negative(message);
		}

		var passwordHash = cryptoService.Hash(userData.Password!);
		await accountRepository.RegisterNewAccountAsync(userData.Username!, passwordHash, userData.Rank).ConfigureAwait(false);

		return Result<string>.Positive("Success");
	}

	public async Task<Result<Player?>> Login(LoginData loginData)
	{
		if (!await accountRepository.AccountExists(loginData.Username).ConfigureAwait(false))
		{
			return Result<Player?>.Negative("Spielername nicht vergeben");
		}

		Player player;
		try
		{
			player = await accountRepository.GetPlayerFromCredentials(loginData.Username, cryptoService.Hash(loginData.Password)).ConfigureAwait(false);
		}
		catch (InvalidCredentialException e)
		{
			return Result<Player?>.Negative(e.Message);
		}

		var token = await accountRepository.GetNewTokenForPlayer(loginData.Username).ConfigureAwait(false);
		return Result<Player?>.Positive(player, token.ToString());
	}

	public async Task<bool> IsAuthorized(Guid loginToken) => await accountRepository.ValidateToken(loginToken).ConfigureAwait(false);

	public bool IsAuthorized() => currentPlayerId.HasValue;

	public async Task Logout()
	{
		if (currentToken != null)
			await accountRepository.RemoveLoginToken(currentToken.Value).ConfigureAwait(false);
	}

	public async Task LogoutAll()
	{
		if (currentToken != null)
			await accountRepository.RemoveAllTokensFor(currentToken.Value).ConfigureAwait(false);
	}

	public async Task<Result<string>> Delete(DeletionData deletionData, string token)
	{
		if (string.IsNullOrWhiteSpace(deletionData.Username))
		{
			return Result<string>.Negative("No user specified.");
		}

		if (string.IsNullOrWhiteSpace(deletionData.Password))
		{
			return Result<string>.Negative("Incorrect username or password.");
		}

		var passwordHash = cryptoService.Hash(deletionData.Password);
		if (!await accountRepository.ValidateActiveUserData(deletionData.Username, passwordHash, Guid.Parse(token)).ConfigureAwait(false))
		{
			return Result<string>.Negative("Wrong password or not logged in.");
		}

		await accountRepository.DeleteAccount(deletionData.Username).ConfigureAwait(false);

		return Result<string>.Positive(deletionData.Username, "Successfully deleted user.");
	}

	public async Task<Result> ChangePlayerName(string newName)
	{
		if (!IsAuthorized()) return Result.Negative("Player must be logged in to change their name.");

		if (!UserDataValidator.IsUsernameValid(newName, out var msg)) return Result.Negative(msg);

		if (await accountRepository.AccountExists(newName).ConfigureAwait(false)) return Result.Negative($"Name '{newName}' is already taken.");

		await accountRepository.ChangeName(currentPlayerId!.Value, newName).ConfigureAwait(false);
		return Result.Positive();
	}

	public async Task<Result> ChangePlayerRank(Rank newRank)
	{
		if (!IsAuthorized()) return Result.Negative("Player must be logged in to change their rank.");

		await accountRepository.ChangeRank(currentPlayerId!.Value, newRank).ConfigureAwait(false);
		return Result.Positive();
	}

	public short GetCurrentPlayerId()
	{
		return !IsAuthorized()
			? ThrowForAuth()
			: currentPlayerId!.Value;
	}
}
