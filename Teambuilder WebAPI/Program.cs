using Microsoft.OpenApi.Models;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.ResponseCompression;
using Teambuilder_WebAPI.ActiveAdapters;
using Teambuilder_WebAPI.ActiveAdapters.Hubs;
using Teambuilder_WebAPI.Services;
using TeambuilderLibrary.Contracts.Account;
using TeambuilderLibrary.Contracts.Team;
using TeambuilderLibrary.Contracts;
using TeamBuilderServer;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllersWithViews();
builder.Services.AddRazorPages();
builder.Services.AddScoped<ExceptionHandler>();
builder.Services.AddCors(opts =>
	opts.AddDefaultPolicy(policy => policy.AllowAnyMethod().AllowAnyHeader().WithOrigins("http://localhost", "https://localhost:5001", "https://teambuilder.z6.web.core.windows.net")));
builder.Services.AddMvcCore()
	.AddJsonOptions(options =>
	{
		options.JsonSerializerOptions.PropertyNamingPolicy = null;
		options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
	})
	.AddApiExplorer();
builder.Services.AddSignalR();
builder.Services.AddResponseCompression(opts
	=> opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(["application/octet-stream"]));

builder.Services.AddSwaggerGen(c => c.SwaggerDoc("v1", new OpenApiInfo { Title = "API", Version = "v1" }));

builder.Services.AddDatabase(builder.Configuration.GetConnectionString("TeambuilderDatabase"))
	.AddSingleton<ISecretSantaService, SecretSantaService>()
	.AddScoped<IAccountService, AccountService>()
	.AddScoped<ITeamService, TeamService>();
builder.Services.AddHostedService<Shell>();

var app = builder.Build();

// Make sure the database is up-to-date
app.Services.UpdateDatabase();

app.UseResponseCompression();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
	app.UseWebAssemblyDebugging();
}
else
{
	// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
	app.UseHsts();
}

app.UseHttpsRedirection();
app.UseMiddleware<ExceptionHandler>();

app.UseBlazorFrameworkFiles();
app.UseStaticFiles();

app.UseSwagger()
	.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "API v1"))
	.UseRouting()
	.UseCors();


app.MapControllers();
app.MapHub<TeambuilderHub>("/signalr");
app.MapFallbackToFile("index.html");

app.Run();