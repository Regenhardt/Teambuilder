using Microsoft.AspNetCore.Mvc;
using TeambuilderLibrary.Contracts.Team;
using TeambuilderLibrary.Models;

// ReSharper disable InvalidXmlDocComment

namespace Teambuilder_WebAPI.ActiveAdapters.ApiControllers;

[Route("api/Teams")]
public class TeamsController(ITeamService service) : ControllerBase
{

	/// <summary>
	/// GETs all teams.
	/// </summary>
	/// <returns>All teams.</returns>
	[HttpGet]
    public async IAsyncEnumerable<Team> GetTeamsAsync()
    {
        await foreach (var team in service.GetAllTeams()) yield return team;
    }

    // ****************** Irgendwann als Fallback von der RTC einbauen ***************
    /// <summary>
    /// GETs a single team.
    /// </summary>
    /// <param name="name">Name of the team to get.</param>
    /// <returns>A team.</returns>
    //[HttpGet("{name}", Name = "GetTeam")]
    //public Team Get([FromRoute]string name)
    //{
    //	return teamService.GetTeam(name);
    //}

    ///// <summary>
    ///// Creates a team.
    ///// </summary>
    ///// <param name="value">The desired team name.</param>
    ///// <returns>409 Conflict if the teamname already exists, or 200 Ok if the team was successfully created.</returns>
    //[HttpPost]
    //public IActionResult Post([FromBody]string value)
    //{
    //	string msg;
    //	if( (msg = teamService.CreateTeam(value, Token)) == string.Empty)
    //		return Ok();

    //	return Conflict(msg);
    //}

    ///// <summary>
    ///// Update a Team. Change name or add/remove players.
    ///// </summary>
    ///// <param name="teamName">Name of the team.</param>
    ///// <param name="value">Team object.</param>
    //[HttpPut("{name}")]
    //public async Task<IActionResult> Put([FromRoute]string teamName, [FromBody]Team value)
    //{
    //	Player player = await accountService.AuthenticateUser(Request.Headers["token"]);
    //	if (player == null) return Unauthorized();
    //	Team team = teamService.GetTeam(teamName);
    //	if (team == null) return StatusCode(StatusCodes.Status418ImATeapot, $"Team {teamName} doesn't exist.");
    //	return Ok();
    //}

    ///// <summary>
    ///// Deletes a team.
    ///// </summary>
    ///// <param name="name">Name of the team to delete.</param>
    ///// <returns>200 Ok if the team was deleted, 401 Unauthorized if the token is invalid.</returns>
    //[HttpDelete("{name}")]
    //public async Task<IActionResult> Delete([FromRoute]string name)
    //{
    //	// Logged in?
    //	Player player = await accountService.AuthenticateUser(Request.Headers["token"]);
    //	if (player == null) return Unauthorized();
    //	// Owner?
    //	Team team = teamService.GetAllTeams().Single(t => t.Owner.Name == player.Name);
    //	//if (player.Equals(team.Owner)) return Unauthorized();
    //	// Delete team.
    //	teamService.DeleteTeam(name);
    //	return Ok();
    //}
}
