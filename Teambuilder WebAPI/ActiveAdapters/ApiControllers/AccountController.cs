using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using TeambuilderLibrary.Contracts;
using TeambuilderLibrary.Contracts.Account;
using TeambuilderLibrary.Models;

namespace Teambuilder_WebAPI.ActiveAdapters.ApiControllers;

[Route("api/[controller]")]
[ApiController]
public class AccountController(IAccountService service) : ControllerBase
{

	/// <summary>
	///     Register a new user account
	/// </summary>
	/// <param name="userData">Username and password./></param>
	/// <returns>"Success" in Data or error in Message.</returns>
	[HttpPost("register")]
	public async Task<Result<string>> Register([FromBody] RegistrationData userData) => await service.RegisterNewUserAsync(userData);

	[HttpGet("login")]
	public async Task<Result<Player>> Validate([FromHeader(Name = Constants.TokenKey)] Guid token)
	{
		try
		{
			await service.Authorize(token);
		}
		catch (SecurityTokenValidationException e)
		{
			return Result<Player>.Negative(e.Message);
		}
		return Result<Player>.Positive(service.CurrentPlayer!);
	}

	/// <summary>
	///		Log in to Teambuilder.
	/// </summary>
	/// <param name="loginData">Username and password</param>
	/// <returns>Player as Payload and token in header <see cref="Constants.TokenKey"/>.</returns>
	[HttpPost("login")]
	public async Task<Result<Player?>> Login([FromBody] LoginData loginData)
	{
		var result = await service.Login(loginData);

		if (result.Success) Response.Headers[Constants.TokenKey] = result.Message!;
		Console.WriteLine(result);
		return result;
	}

	/// <summary>
	///     Log out of Teambuilder. You can assume to be logged out after this.
	/// </summary>
	/// <param name="token">The token of the client logging out.</param>
	[HttpDelete("logout")]
	public async Task Logout([FromHeader(Name = Constants.TokenKey)] string? token)
	{
		if (token == null) return;
		await service.Authorize(Guid.Parse(token));
		await service.Logout();
	}

	/// <summary>
	///     Log out of Teambuilder. You can assume to be logged out after this.
	/// </summary>
	/// <param name="token">The token of the client logging out.</param>
	[HttpDelete("logout-all")]
	public async Task LogoutAll([FromHeader(Name = Constants.TokenKey)] string? token)
	{
		if (token == null) return;
		await service.Authorize(Guid.Parse(token));
		await service.LogoutAll();
	}

	/// <summary>
	/// Delete an account.
	/// </summary>
	/// <param name="deletionData">The account to delete</param>
	/// <param name="token">Current login token of account to delete</param>
	/// <returns></returns>
	[HttpDelete("delete")]
	public async Task<Result<string>> Delete([FromHeader(Name = Constants.TokenKey)] string? token, [FromBody] DeletionData? deletionData)
	{
		if (string.IsNullOrWhiteSpace(token)) return Result<string>.Negative("You have to be logged in to delete your account.");

		if (deletionData == null) return Result<string>.Negative("Please provide data for which account to delete.");

		return await service.Delete(deletionData, token);
	}

	/// <summary>
	/// Change the name of the current player.
	/// </summary>
	/// <param name="token">Must be logged in.</param>
	/// <param name="newName">The name to change to.</param>
	/// <returns>Result indicates whether or not the change was successful with an error message if applicable.</returns>
	[HttpPatch("name")]
	public async Task<Result> ChangeName([FromHeader(Name = Constants.TokenKey)]
			Guid token, [FromBody] string newName)
	{
		try
		{
			await Authorize(token);
		}
		catch (SecurityTokenValidationException exception)
		{
			return Result.Negative(exception.Message);
		}
		return await service.ChangePlayerName(newName);
	}

	/// <summary>
	/// Change the rank of the current player.
	/// </summary>
	/// <param name="token">Must be logged in.</param>
	/// <param name="newRank">The rank to change to.</param>
	/// <returns>Result indicates whether or not the change was successful with an error message if applicable.</returns>
	[HttpPatch("rank")]
	public async Task<Result> ChangeRank([FromHeader(Name = Constants.TokenKey)]
			Guid token, [FromBody] Rank newRank)
	{
		try
		{
			await Authorize(token);
		}
		catch (SecurityTokenValidationException exception)
		{
			return Result.Negative(exception.Message);
		}
		return await service.ChangePlayerRank(newRank);
	}

	private async Task Authorize(Guid token) => await service.Authorize(token);
}
