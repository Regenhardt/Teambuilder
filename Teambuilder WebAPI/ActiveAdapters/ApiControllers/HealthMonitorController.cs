using Microsoft.AspNetCore.Mvc;
using TeambuilderLibrary.Contracts.Account;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Teambuilder_WebAPI.ActiveAdapters.ApiControllers;

[Route("api/Health")]
public class HealthMonitorController(IServiceProvider serviceProvider) : ControllerBase
{
	private readonly IServiceProvider serviceProvider = serviceProvider;

	[HttpGet]
	public string Get() => "OK";

	[HttpGet("db")]
	public async Task<string> DatabaseAsync()
	{
		var repo = serviceProvider.GetRequiredService<IAccountRepository>();
		try
		{
			_ = await repo.AccountExists("Dummy, value doesn't matter");
		}
		catch (Exception e)
		{
			return e.Message;
		}
		return "OK";
	}
}
