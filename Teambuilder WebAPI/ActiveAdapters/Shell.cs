using System.Globalization;
using TeambuilderLibrary.Contracts.Team;

namespace Teambuilder_WebAPI.ActiveAdapters;

public class Shell : BackgroundService
{
	private bool quitRequested;
	private readonly Dictionary<string, Func<Task<string>>> Commands = [];
	private IEnumerable<string> Choices => Commands.Keys;
	private readonly ITeamService teamService;
	private readonly IServiceScope scope;

	public Shell(IServiceProvider sp)
	{
		scope = sp.CreateScope();
		sp = scope.ServiceProvider;
		teamService = sp.GetRequiredService<ITeamService>();

		Commands["D - GetDate"] = () => Task.FromResult(DateTime.Now.ToString("u", CultureInfo.InvariantCulture));
		Commands["T - Get all teams"] = GetAllTeams;
		Commands["Q - Quit"] = Quit;
	}

	#region [ Looping ]

	protected override async Task ExecuteAsync(CancellationToken stoppingToken) => await Loop(stoppingToken).ConfigureAwait(false);


	private async Task Loop(CancellationToken stoppingToken)
	{
		char chosenOption;
		try
		{

			while (!stoppingToken.IsCancellationRequested && !quitRequested && (chosenOption = await GetChoice()) != 'Q')
			{
				Console.WriteLine(await Commands[Commands.Keys.Single(key => key[0] == chosenOption)]());
			}
		}
		catch (PlatformNotSupportedException exc)
		{
			Console.WriteLine(exc.Message);
		}
		Console.WriteLine("Shutting down shell loop");
	}

	private async Task<char> GetChoice()
	{
		char chosenOption;
		Console.WriteLine("Menu:");
		do
		{
			foreach (var choice in Choices)
			{
				Console.WriteLine(choice);
			}

			var input = await Task.Run(Console.ReadLine);
			if (input == null) throw new PlatformNotSupportedException("No input stream available");
			chosenOption = char.ToUpper(input[0]);
			Console.WriteLine();
		} while (!IsValidChoice(chosenOption));

		return chosenOption;
	}

	private bool IsValidChoice(char c) => Choices.Any(opt => opt[0] == c);

	public override void Dispose()
	{
		GC.SuppressFinalize(this);
		scope.Dispose();
		base.Dispose();
	}

	#endregion

	#region [ Commands ]

	private async Task<string> GetAllTeams()
	{
		return string.Join(Environment.NewLine,
			await teamService.GetAllTeams().Select(t => t.ToString()).ToArrayAsync());
	}

	private Task<string> Quit()
	{
		quitRequested = true;
		return Task.FromResult("Bye");
	}

	#endregion
}
