using Microsoft.AspNetCore.SignalR;
using TeambuilderLibrary.Contracts;
using TeambuilderLibrary.Contracts.Account;
using TeambuilderLibrary.Contracts.Hub;
using TeambuilderLibrary.Contracts.Team;
using TeambuilderLibrary.Models;

namespace Teambuilder_WebAPI.ActiveAdapters.Hubs;

public class TeambuilderHub : Hub<IClientHub>, IServerHub
{
	/// <summary>
	/// A property to be used by the hub, redirecting the data from and to the Context.
	/// This will allow for properties to be used as user-scoped.
	/// </summary>
	/// <typeparam name="T">The type of data.</typeparam>
	private class Property<T>(Hub hub, string name)
	{
		private readonly Hub hub = hub;
		private readonly string key = name;

#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
		private T Value => (T)(hub.Context.Items.TryGetValue(key, out object o) ? o : null)!;
#pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.
		internal void Set(T value) => hub.Context.Items[key] = value!;

		public static implicit operator T(Property<T> prop) => prop.Value;
	}

	private Guid? Token => token;
	private readonly Property<Guid?> token;

	private readonly IAccountService accountService;
	private readonly ITeamService teamService;
	private readonly ISecretSantaService santaService;

	public TeambuilderHub(IAccountService accountService, ITeamService teamService, ISecretSantaService santaService)
	{
		token = new Property<Guid?>(this, nameof(token));

		this.accountService = accountService;
		this.teamService = teamService;
		this.teamService.OnPatchTeam += PatchTeam;
		this.teamService.OnNewTeam += PushTeam;
		this.teamService.OnDeleteTeam += RemoveTeam;
		this.santaService = santaService;
	}

	/// <summary>
	/// Logs in to join permanent two-way communication.
	/// Adds the user to their own group.
	/// </summary>
	/// <remarks>
	/// Idempotent
	/// </remarks>
	/// <param name="loginToken">Token received when logging in with credentials.</param>
	/// <returns>Whether or not the token is valid.</returns>
	public async Task<Result<string>> Login(Guid loginToken)
	{
		if (await accountService.IsAuthorized(loginToken).ConfigureAwait(false))
		{
			await accountService.Authorize(loginToken);
			Console.WriteLine($"Client {Context.ConnectionId} logged in for token {loginToken}");
			token.Set(loginToken);
			await Groups.AddToGroupAsync(Context.ConnectionId, $"player-{accountService.CurrentPlayer!.Name}");
			return Result<string>.Positive("Connection established");
		}

		Console.WriteLine($"Client {Context.ConnectionId} failed to log in.");
		return Result<string>.Negative("Not logged in");
	}

	#region [ Team: Incoming ]

	public async Task SubscribeToAndReceiveTeamsAsync()
	{
		if (Token.HasValue)
		{
			await accountService.Authorize(Token.Value);
		}

		await foreach (var team in teamService.GetAllTeams().ConfigureAwait(false))
		{
			if (team.Members.Contains(accountService.CurrentPlayer!))
			{
				_ = Groups.AddToGroupAsync(Context.ConnectionId, team.Name);
			}

			await SendNewTeamToCallerAsync(team).ConfigureAwait(false);
		}
	}

	public async Task<Result> JoinTeam(string teamName)
	{
		await accountService.Authorize(Token!.Value);
		Console.WriteLine($"JoinTeam by {Context.ConnectionId} using token {Token}");
		return await teamService.JoinTeam(teamName).ConfigureAwait(false);
	}

	public async Task<Result> LeaveTeam(string teamName)
	{
		await accountService.Authorize(Token!.Value);
		Console.WriteLine($"LeaveTeam by {Context.ConnectionId} using token {Token}");
		return await teamService.LeaveTeam(teamName);
	}

	public async Task<Result> CreateTeam(string teamName)
	{
		await accountService.Authorize(Token!.Value);
		Console.WriteLine($"CreateTeam {teamName} by {Context.ConnectionId} using token {Token}");
		return await teamService.CreateTeam(teamName);
	}

	public async Task<Result> DeleteTeam(string teamName)
	{
		await accountService.Authorize(Token!.Value);
		await teamService.DeleteTeam(teamName);
		return Result.Positive();
	}

	#endregion

	#region [ Team: Outgoing ]

	private async Task SendNewTeamToCallerAsync(Team team) => await Clients.Caller.ReceiveTeam(team).ConfigureAwait(false);

	private async void PatchTeam(string teamName, Team team) => await Clients.All.PatchTeam(teamName, team);

	private async void PushTeam(Team team) => await Clients.All.ReceiveTeam(team);

	private async void RemoveTeam(string teamname) => await Clients.All.RemoveTeam(teamname);

	#endregion

	public Task SendChatMessage(ChatMessage message)
	{
		Clients.Others.ReceiveMessage(message).ConfigureAwait(false);
		return Task.CompletedTask;
	}

	#region [ Misc ]

	public async Task StartSecretSantaForTeam(Team team)
	{
		var santas = this.santaService.GetSecretSantas(team);
		await santas.AsParallel().Select(s => TellPlayerTheirSantee(s.Key, s.Value)).Aggregate((a, b) => Task.WhenAll(a, b));
	}

	private async Task TellPlayerTheirSantee(Player santa, Player santee)
	{
		Console.WriteLine($"Sending info about santa {santa} to everyone");
		await GetClient(santa.Name).SanteeIs(santee);
	}

	private IClientHub GetClient(string playerName) => Clients.Group($"player-{playerName}");

	#endregion
}

// Sequenz vielleicht später implementieren:
// 1) Client triggert Server
// 2) Server blockt -> events in queue
// 3) Server schickt State
// 4) Server löst Block -> queue zum Client
// 5) Kein Block -> events zum Client
// Abhängig vom client state, also dort?