using System.ComponentModel.DataAnnotations;

namespace TeambuilderLibrary.Contracts.Account;

public class PasswordValidatorAttribute : ValidationAttribute
{
	protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
	{
		return UserDataValidator.IsPasswordValid(value as string, out var err)
			? ValidationResult.Success
			: new ValidationResult(err, [validationContext.DisplayName]);
	}
}
