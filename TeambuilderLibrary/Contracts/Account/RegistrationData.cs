using JetBrains.Annotations;
using TeambuilderLibrary.Models;

namespace TeambuilderLibrary.Contracts.Account;

public class RegistrationData
{
	[PublicAPI]
	[UsernameValidator]
	public string? Username { get; set; }
	[PublicAPI]
	[PasswordValidator]
	public string? Password { get; set; }
	[PublicAPI]
	public Rank Rank { get; set; }
}
