namespace TeambuilderLibrary.Contracts.Account;

public class LoginData
{
	public string Username { get; set; } = null!;

	public string Password { get; set; } = null!;

	public bool StayLoggedIn { get; set; }
}
