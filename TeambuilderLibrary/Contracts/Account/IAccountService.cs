using TeambuilderLibrary.Models;

namespace TeambuilderLibrary.Contracts.Account;

public interface IAccountService
{
	/// <summary>
	/// Registers a new user.
	/// </summary>
	/// <param name="userData"></param>
	/// <returns></returns>
	Task<Result<string>> RegisterNewUserAsync(RegistrationData userData);

	Task Authorize(Guid token);
	/// <summary>
	/// Logs a user into Teambuilder.
	/// </summary>
	/// <param name="loginData"></param>
	/// <returns>Result containing the logged in player as payload and the token as message.</returns>
	/// <exception cref="ArgumentException">Throws on wrong username or password.</exception>
	Task<Result<Player?>> Login(LoginData loginData);

	Task<bool> IsAuthorized(Guid loginToken);

	/// <summary>
	/// Log out of Teambuilder. 
	/// </summary>
	Task Logout();

	/// <summary>
	/// Log out of all your devices. No feedback, you can assume everyone to be logged out of your account after this.
	/// </summary>
	/// <returns></returns>
	Task LogoutAll();

	/// <summary>
	/// Delete a user from the Teambuilder database. You can only delete your own user.
	/// </summary>
	/// <param name="deletionData">The player to delete.</param>
	/// <param name="token">Login token from the user being deleted.</param>
	/// <returns>Whether or not the deletion was successful. Provides a message if not successful.</returns>
	Task<Result<string>> Delete(DeletionData deletionData, string token);

	/// <summary>
	/// Change a players name.
	/// </summary>
	/// <param name="newName">The name to change to</param>
	/// <returns></returns>
	Task<Result> ChangePlayerName(string newName);

	/// <summary>
	/// Change a players rank.
	/// </summary>
	/// <param name="newRank">The rank to change to</param>
	/// <returns></returns>
	Task<Result> ChangePlayerRank(Rank newRank);

	short GetCurrentPlayerId();
	Player? CurrentPlayer { get; }
}
