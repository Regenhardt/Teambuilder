namespace TeambuilderLibrary.Contracts.Account;

public class DeletionData
{
	public string? Username { get; set; }
	public string? Password { get; set; }
}
