using TeambuilderLibrary.Models;

namespace TeambuilderLibrary.Contracts.Account;

public interface IAccountRepository
{
	Task<bool> AccountExists(string userName);
	Task RegisterNewAccountAsync(string username, byte[] passwordHash, Rank rank);
	Task<Player> GetPlayer(string username);
	Task<Player> GetPlayerFromCredentials(string username, byte[] passwordHash);
	Task<Guid> GetNewTokenForPlayer(string username);
	Task RemoveLoginToken(Guid guidToken);
	Task<bool> ValidateActiveUserData(string deletionDataUsername, byte[] passwordHash, Guid token);
	Task DeleteAccount(string username);
	Task ChangeName(short playerId, string newName);
	Task ChangeRank(short playerId, Rank newRank);
	Task<bool> ValidateToken(Guid loginToken);
	Task<Player> GetPlayerFromToken(Guid token);
	Task<short> GetPlayerIdFromToken(Guid currentToken);
	Task RemoveAllTokensFor(Guid currentToken);
}
