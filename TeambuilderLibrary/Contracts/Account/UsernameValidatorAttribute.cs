using System.ComponentModel.DataAnnotations;

namespace TeambuilderLibrary.Contracts.Account;

public class UsernameValidatorAttribute : ValidationAttribute
{
	protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
	{
		return UserDataValidator.IsUsernameValid(value as string, out var err)
			? ValidationResult.Success
			: new ValidationResult(err, [validationContext.DisplayName]);
	}
}
