using TeambuilderLibrary.Models;

namespace TeambuilderLibrary.Contracts.Hub;

public interface IClientHub
{
	public Task ReceiveTeam(Models.Team team);
	public Task ReceiveMessage(ChatMessage msg);
	public Task PatchTeam(string teamName, Models.Team team);
	Task RemoveTeam(string teamname);
	Task SanteeIs(Player santee);
}
