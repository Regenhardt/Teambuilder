using TeambuilderLibrary.Models;

namespace TeambuilderLibrary.Contracts.Hub;

public interface IServerHub
{
	/// <summary>
	/// Logs in to join permanent two-way communication.
	/// </summary>
	/// <remarks>
	/// Idempotent
	/// </remarks>
	/// <param name="loginToken">Token received when logging in with credentials.</param>
	/// <returns>Whether or not the token is valid.</returns>
	public Task<Result<string>> Login(Guid loginToken);
	public Task SubscribeToAndReceiveTeamsAsync();
	public Task SendChatMessage(ChatMessage message);
	public Task<Result> LeaveTeam(string teamName);
	Task StartSecretSantaForTeam(Models.Team team);
	public Task<Result> JoinTeam(string teamName);
	public Task<Result> CreateTeam(string teamName);
	Task<Result> DeleteTeam(string teamName);
}
