using TeambuilderLibrary.Models;

namespace TeambuilderLibrary.Contracts.Team;

public interface ITeamService
{
	/// <summary>
	/// Fired when a new team got added to the roster.
	/// </summary>
	event Action<Models.Team> OnNewTeam;

	/// <summary>
	/// Fired when a team, identified by name, got updated.
	/// </summary>
	event Action<string, Models.Team>? OnPatchTeam;

	/// <summary>
	/// Fired when a team, identified by name, got deleted.
	/// </summary>
	event Action<string>? OnDeleteTeam;

	/// <summary>
	/// Returns all Teams including their players.
	/// </summary>
	/// <returns></returns>
	IAsyncEnumerable<Models.Team> GetAllTeams();

	/// <summary>
	/// Try to create a new Team.
	/// </summary>
	/// <remarks>Needs prior authorization.</remarks>
	/// <param name="name">Desired name for the new Team.</param>
	/// <returns>Empty string if the team was successfully created, or an error message.</returns>
	Task<Result> CreateTeam(string name);

	/// <summary>
	/// Try to delete a Team.
	/// </summary>
	/// <remarks>Needs prior authorization.</remarks>
	/// <param name="name">The name of the Team to delete.</param>
	/// <returns>Whether or not the Team could be deleted.</returns>
	Task DeleteTeam(string name);

	/// <summary>
	/// Try to change the name of a Team.
	/// </summary>
	/// <param name="oldName">Old name of the Team.</param>
	/// <param name="newName">New name of the Team.</param>
	/// <returns>Whether or not the Team could be updated.</returns>
	void UpdateTeamName(string oldName, string newName);


	/// <summary>
	/// Join a Player to a Team.
	/// </summary>
	/// <remarks>Needs prior authorization.</remarks>
	/// <param name="teamName">Name of the Team.</param>
	Task<Result> JoinTeam(string teamName);

	/// <summary>
	/// Remove a player from a team.
	/// </summary>
	/// <remarks>Needs prior authorization.</remarks>
	/// <param name="teamName"></param>
	/// <returns></returns>
	Task<Result> LeaveTeam(string teamName);
}
