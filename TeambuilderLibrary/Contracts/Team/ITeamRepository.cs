using TeambuilderLibrary.Models;

namespace TeambuilderLibrary.Contracts.Team;

public interface ITeamRepository
{
	IAsyncEnumerable<Models.Team> GetAllTeams();
	Task<Result> AddPlayerToTeam(short playerId, int teamId);
	Task<Result<int>> GetTeamIdByName(string teamName);
	Task<Models.Team> GetTeam(int teamId);
	Task<Result> RemovePlayerFromTeam(short playerId, string teamName);
	Task<Models.Team> GetTeam(string teamName);
	Task<Result<Models.Team>> CreateNewTeam(short ownerId, string teamName);
	Task DeleteTeam(short ownerId, string teamName);
}
