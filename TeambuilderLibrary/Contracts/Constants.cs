namespace TeambuilderLibrary.Contracts;

public static class Constants
{
	public const string TokenKey = "token";
	public const string UserNameKey = "username";
	public const string PasswordKey = "password";
	public const int MaxPasswordLength = 50;
}
