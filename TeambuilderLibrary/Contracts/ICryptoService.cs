namespace TeambuilderLibrary.Contracts;

public interface ICryptoService
{
	byte[] Hash(string password);
}
