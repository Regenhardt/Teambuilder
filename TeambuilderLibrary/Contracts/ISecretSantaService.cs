using TeambuilderLibrary.Models;

namespace TeambuilderLibrary.Contracts;

public interface ISecretSantaService
{
	public Dictionary<Player, Player> GetSecretSantas(Models.Team team);
}
