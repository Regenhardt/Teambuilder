namespace TeambuilderLibrary.Extensions;

public static class General
{
	public static bool IsNullOrEmpty(this string? s) => string.IsNullOrEmpty(s);

	public static T As<T>(this object o) where T : class => (T)o;
}
