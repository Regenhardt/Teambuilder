using TeambuilderLibrary.Models;

namespace TeambuilderLibrary.Extensions;

public static class TaskExtensions
{
	public static async Task<T> Data<T>(this Task<Result<T>> result) => (await result).Data;

	public static async void Forget(this Task t) => await t.ConfigureAwait(false);

	public static async void Forget<T>(this Task<T> t) => await t.ConfigureAwait(false);
}
