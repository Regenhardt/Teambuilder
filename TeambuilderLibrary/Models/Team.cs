namespace TeambuilderLibrary.Models;

public class Team
{
	public string Name { get; set; } = "Team1";
	public Player Owner { get; set; } = null!;
	public IList<Player> Members { get; set; } = new List<Player>(5);

	public override string ToString() => $"Team {Name} of {Owner}, Members: {Members.Count}";
}
