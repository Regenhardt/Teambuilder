using JetBrains.Annotations;

namespace TeambuilderLibrary.Models;

public struct Result(bool success, string? message = null)
{
	public static Result Positive(string? message = null) => new(true, message);

	public static Result Negative(string message) => new(false, message);

	[PublicAPI] public bool Success { get; set; } = success;

	[PublicAPI] public string? Message { get; set; } = message;
}

// Setter nur sind public, da die Klasse deserialisiert werden muss.
public struct Result<T>
{
	private Result(bool success, T data, string? message = null)
	{
		Success = success;
		Data = data;
		Message = message;
	}

	[PublicAPI] public bool Success { get; set; }

	[PublicAPI] public string? Message { get; set; }

	[PublicAPI] public T Data { get; set; }

	public static Result<T> Positive(T data, string? message = null) => new(true, data, message);

	public static Result<T> Negative(string message)
	{
		return new()
		{
			Success = false,
			Message = message
		};
	}

	public override readonly string ToString() => $"Result: {Success}, Data: {Data}, Message: {Message}";
}
