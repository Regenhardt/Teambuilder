using JetBrains.Annotations;

namespace TeambuilderLibrary.Models;

[PublicAPI]
public class Player
{
	public string Name { get; set; }
	public Rank Rank { get; set; }

#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
	public Player()
#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
	{
	}
	public Player(string name, Rank rank)
	{
		Name = name;
		Rank = rank;
	}

	public override string ToString() => $"{Name}, {Rank}";

	public override bool Equals(object? obj) => this == obj as Player;

	public override int GetHashCode() => HashCode.Combine(Name, (int)Rank);

	public static bool operator ==(Player? self, Player? other)
	{
		return ReferenceEquals(self, other) ||
			   (self is not null && other is not null
			   && self.Name == other.Name);
	}

	public static bool operator !=(Player? self, Player? other) => !(self == other);
}
