namespace TeambuilderLibrary.Models;

public enum Rank
{
	Unranked,
	Herald,
	Guardian,
	Crusader,
	Archon,
	Legend,
	Ancient,
	Divine
}
