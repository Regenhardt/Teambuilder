namespace TeambuilderLibrary.Models;

public class ChatMessage(string sender, string content)
{
	public static ChatMessage New => new("Anonymous", string.Empty);

	public string Sender { get; set; } = sender;
	public string Content { get; set; } = content;

	public override string ToString() => $"{{{Sender}: {Content}}}";
}
